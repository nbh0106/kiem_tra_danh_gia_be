FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7


# RUN pip install fastapi uvicorn
# COPY course_exams_sever.py .
COPY requirements.txt .
# COPY test_mysql.py .
# WORKDIR /app

# install owncloud
RUN apt update
RUN apt list --upgradable
RUN apt-get install sudo -y
RUN pip install --upgrade pip

RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN bash get-docker.sh

RUN apt install owncloud-client-cmd -y

# RUN sudo sh -c "echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/desktop:/daily:/2.6/Ubuntu_20.04/ /' > /etc/apt/sources.list.d/isv:ownCloud:desktop:daily:2.6.list"
RUN echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/desktop/Linux_Mint_19/ 10 /' | sudo  tee /etc/apt/sources.list.d/isv:ownCloud:desktop.list

# RUN wget -qO - https://download.opensuse.org/repositories/isv:ownCloud:desktop:daily:2.6/Ubuntu_20.04/Release.key | apt-key add -
RUN curl -fsSL https://download.opensuse.org/repositories/isv:ownCloud:desktop/Linux_Mint_19/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/isv:ownCloud:desktop.gpg > /dev/null

RUN apt install owncloud-client -y

COPY requirements.txt .
RUN python -m venv env
RUN pip install -r requirements.txt

COPY ./app /exams-be/app 
COPY course_exams_server.py /exams-be 
ADD python_test.py /exams-be 
RUN ls /exams-be/
RUN apt install cmake -y 
RUN git clone https://github.com/jarro2783/cxxopts.git
RUN cd cxxopts && mkdir build && cd build && cmake .. && make && make install 
RUN mkdir /exams-be/src && \ 
    mkdir /exams-be/bin && \
    mkdir /exams-be/src/cpp && \
    mkdir  /exams-be/src/java && \
    mkdir  /exams-be/src/python &&\
    mkdir /exams-be/bin/cpp && \
    mkdir  /exams-be/bin/java && \
    mkdir  /exams-be/bin/python
#install jdk to compile java program
RUN yes | sudo apt install default-jdk
#RUN apt-get update 
#RUN apt-get install vim
CMD . env/bin/activate && \
        cd /exams-be && uvicorn app.jwt:app --host 0.0.0.0 --port="${PORT}"


# CMD bash