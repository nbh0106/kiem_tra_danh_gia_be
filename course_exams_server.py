import uvicorn

if __name__ == "__main__":
    uvicorn.run("app.jwt:app", host="0.0.0.0", port=8890, workers=4, reload=True)

