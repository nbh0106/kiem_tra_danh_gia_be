
import aiomysql
import mysql.connector
import time 
import asyncio

async def create_connection(i):
    print('Start', i)
    pool = await aiomysql.create_pool(host='127.0.0.1', port=3307,
                                    user='root', password='123456789',
                                    db='moodle')

    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute('select * from mdl_roles')
        

    await asyncio.sleep(100)
    print('Finish', i)

async def main():
    task_list = []
    for i in range(150):
        task1 = asyncio.create_task(
            create_connection(i))
        task_list.append(task1)
    print(f"started at {time.strftime('%X')}")

    # Wait until both tasks are completed (should take
    # around 2 seconds.)
    for task in task_list:
        await task
    print(f"finished at {time.strftime('%X')}")

asyncio.run(main())
    
