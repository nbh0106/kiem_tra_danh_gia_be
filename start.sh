docker run --rm -d\
    --network host \
    -v /home/ds2/aiv-resources/kiem_tra_danh_gia_be/app:/exams-be/app \
    -v /data/owncloud:/data/owncloud \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -e PORT=8812    \
    -e PORT_JUPYTER=37000 \
    -e DOMAIN='exams1.aiacademy.edu.vn'   \
    -e SSL_DIR='/home/ds2/aiv-resources/Assiment/exams-jupyter/ssl-cert/exams1.aiacademy.edu.vn'  \
    --name be-exams1  \
    exams_be


    docker run --rm -d \
    --network host \
   -v /home/ds2/aiv-resources/kiem_tra_danh_gia_be/app:/exams-be/app \
    -v /data/owncloud:/data/owncloud \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -e PORT=8813    \
    -e PORT_JUPYTER=39000 \
    -e DOMAIN='exams2.aiacademy.edu.vn'   \
    -e SSL_DIR='/home/ds2/aiv-resources/Assiment/exams-jupyter/ssl-cert/exams2.aiacademy.edu.vn'  \
    --name be-exams2  \
    exams_be
