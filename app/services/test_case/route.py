from typing import List, Optional
from fastapi import APIRouter, Body , Query, Depends, Path,HTTPException,status
from fastapi.encoders import jsonable_encoder
from bson.objectid import ObjectId

from datetime import datetime
from ...setting.responseModle import ErrorResponseModel, ResponseModel, ResponseCreateModel

from .model import InsertSchema, UpdateSchema, GetSchema, GetSchemaAll
from ...database import mgo_db_connector as database
from fastapi.security import HTTPBearer

router = APIRouter()

collection = database.get_collection("test_case")
collection_cauhoi = database.get_collection('cau_hoi')

try:
    
    collection.create_index('ten_cau_hoi')
    collection.create_index('chuoi_gia_tri_dau_vao')
    collection.create_index('chuoi_gia_tri_dau_ra')
    collection.create_index('an')
    collection.create_index('thoi_gian_tao')
except Exception as error:
    print(error)


async def check_valid_condition(condition):
    if condition == None:
        return True 
    return await collection.find_one(condition) != None


async def check_valid(condition):
    return await collection.find_one(condition) != None


@router.post("/", dependencies = [Depends(HTTPBearer())])
async def insert(payload: InsertSchema = Body(...)):
    
    condition, data = payload.get_params()

    if condition is None:
        raise HTTPException(status_code =  405,detail= "chưa nhập đủ thông tin đầu vào")

    if await check_valid(condition):
        raise HTTPException(status_code =  405,detail = "Tên testcase bị trùng")

    result = await collection.insert_one(data)

    return str(result.inserted_id)

@router.get('/cauhoi/{cauhoiid}',dependencies = [Depends(HTTPBearer())],status_code =status.HTTP_200_OK)
async def get_testcase_via_cauhoi_id(cauhoiid:str = Path(...)):
    cauhoi = await collection_cauhoi.find_one({'_id':ObjectId(cauhoiid)})
    data = {}
    if not cauhoi:
        raise HTTPException(status_code=404,detail='not found')
    cauhoi.update({'id':str(cauhoi['_id'])})
    del cauhoi['_id']
    data.update({'cau_hoi':cauhoi})

    list_testcase = []
    list_testcase_db = collection.find({'ma_cau_hoi':cauhoiid })
    
    async for testcase in list_testcase_db:
        testcase['id'] = str(testcase['_id'])
        del testcase['_id']
        list_testcase.append(testcase)
    
    data.update({'list_testcase':list_testcase})
    return data

@router.get('/{cauhoiid}/get_hide_testcase',dependencies = [Depends(HTTPBearer())],status_code =status.HTTP_200_OK)
async def get_testcase_via_cauhoi_id(cauhoiid:str = Path(...)):
    cauhoi = await collection_cauhoi.find_one({'_id':ObjectId(cauhoiid)})
    data = {}
    if not cauhoi:
        raise HTTPException(status_code=404,detail='not found')
    cauhoi.update({'id':str(cauhoi['_id'])})
    del cauhoi['_id']
    data.update({'cau_hoi':cauhoi})

    list_testcase_not_hide = []
    testcase_hide_sum = await collection.count_documents({'ma_cau_hoi':cauhoiid,'an':True })
    list_testcase_db_not_hide = collection.find({'ma_cau_hoi':cauhoiid,'an':False })
    
    async for testcase in list_testcase_db_not_hide:
        testcase['id'] = str(testcase['_id'])
        del testcase['_id']
        list_testcase_not_hide.append(testcase)
    
    
    
    data.update({'list_testcase_not_hide':list_testcase_not_hide})
    data.update({'testcase_hide_sum':testcase_hide_sum})
    return data


@router.get("/", dependencies = [Depends(HTTPBearer())])
async def get(payload: GetSchema = Depends()):
    condition = payload.get_params()
    result = collection.find(condition)

    return await payload.format_mgo(result)

@router.get("/GetAll", dependencies = [Depends(HTTPBearer())])
async def get(payload:GetSchemaAll = Depends()):
    condition = payload.get_params()
    result = collection.find(condition)

    return await payload.format_mgo(result)

@router.put("/{id}", dependencies = [Depends(HTTPBearer())])
async def update(id: str=Path(...), payload: UpdateSchema=Body(...)):

    condition, data = payload.get_params(id)
    if await collection.find_one(condition) is None:
        return ErrorResponseModel(405,f"Lỗi {id} không tồn tại")

    result = await collection.update_one(condition, {"$set": data})
    return True

@router.delete("/{id}", dependencies = [Depends(HTTPBearer())])
async def delete(id:str=Path(...)):
    testcase = await collection.find_one({"_id":ObjectId(id)})
    if testcase == None:
        return ErrorResponseModel(405,"TestCase chưa tồn tại")
    else:
        await collection.delete_one({"_id":ObjectId(id)})
        return ResponseModel("Thành công","Đã xóa Testcase")
