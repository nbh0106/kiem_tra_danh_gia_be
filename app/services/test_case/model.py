from typing import Dict, List, Optional
import time
from fastapi import APIRouter, Body , Query
from bson.objectid import ObjectId
from pydantic import BaseModel,Field
from ...database import mgo_db_connector as database

collection = database.get_collection("test_case")
collection = database.get_collection("cau_hoi")

class InsertSchema(BaseModel):
    ma_cau_hoi: str = Field(...)
    ten_cau_hoi: str = Field(...)
    chuoi_gia_tri_dau_vao: List[Dict[str,str]] = Field(...)
    gia_tri_dau_ra:Dict[str, str]= Field(...)
    an: bool = Field(False)
    
    def get_params(self):

        if(self.ma_cau_hoi=="" or self.chuoi_gia_tri_dau_vao =="" or self.gia_tri_dau_ra ==""):
            condition = None
        else:
            condition= False
        data ={
            **vars(self),
            'thoi_gian_tao':int(time.time())
        }
        
        return condition,data

class GetSchema(BaseModel):
    ma_cau_hoi: str = Field(...)
    an:bool = Field(None)
    def get_params(self):
        condition={}

        if self.ma_cau_hoi != None:
            condition['ma_cau_hoi'] = self.ma_cau_hoi
        if self.an != None:
            condition['an'] = self.an

        return condition

    async def format_mgo(self,data):
        result = []

        async for r in data:
            r['_id'] = str(r['_id'])

            result.append(r)
        return result

        
class GetSchemaAll(BaseModel):
    def get_params(self):
        condition={}

    async def format_mgo(self,data):
        result = []

        async for r in data:
            r['_id'] = str(r['_id'])

            result.append(r)
        return result

class UpdateSchema(BaseModel):
    ma_cau_hoi: Optional[str] = Field(None)
    ten_cau_hoi: Optional[str] = Field(None)
    chuoi_gia_tri_dau_vao: Optional[List[Dict[str,str]]] = Field(None)
    gia_tri_dau_ra:Optional[Dict[str, str]]= Field(None)
    an: bool = Field(False)

    def get_params(self,id):
        data = {}
        condition ={
            "_id":ObjectId(id)
        }

        data={k:v for k,v in self.dict().items() if v is not None}
        return condition,data
