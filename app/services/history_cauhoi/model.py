
from typing import Collection
from typing import Optional,Dict
from bson.objectid import ObjectId

from fastapi import Depends
import time
import json
from ...database import mgo_db_connector as database
from pydantic import BaseModel,EmailStr,Field
from fastapi_jwt_auth import AuthJWT

Collection = database.get_collection("history_cauhoi")


class InsertSchema(BaseModel):
    # user_id: int = Field(...)
    ma_testcase: str = Field(...)
    # input_testcase:Dict[str, str] = Field(...)
    # output_testcase:Dict[str, str] = Field(...)
    output:Dict[str, str] = Field(...)
    console_output: str = Field(...)
    Error_output : str = Field(...)

    # async def get_history_cauhoi_via_is_finished(user_id:int):
    #     record = await database.history_cauhoi.find_one({'user_id':user_id})
    #     if record:
    #         del record['_id']
    #     return record


    def get_params(self, user_id):

        data = {
            # 'input_testcase': self.input_testcase,
            'user_id': user_id,
            'ma_testcase': self.ma_testcase,
            # 'output_testcase': self.output_testcase,
            'output': self.output,
            'console_output': self.console_output,
            'Error_output':self.Error_output,
        }
        print(data)
        return data


class GetScheama(BaseModel):
    user_id : int = Field(...)
    def get_params(self):
        condition={}

        if self.user_id != None:
            condition['user_id'] = self.user_id

        return condition

    async def format_mgo(self,data):
        result = []

        async for r in data:
            r['_id'] = str(r['_id'])

            result.append(r)
        return result

class GetScheamaAll(BaseModel):
    def get_params(self):
        condition={}

    async def format_mgo(self,data):
        result = []

        async for r in data:
            r['_id'] = str(r['_id'])

            result.append(r)
        return result

class UpdateScheama(BaseModel):
    output: Optional[Dict[str,str]]
    console_output: Optional[str]
    Error_output: Optional[str]
    ma_testcase:Optional[str]


    def get_params(self,id):
        data = {}
        condition ={
            "_id":ObjectId(id)
        }

        data={k:v for k,v in self.dict().items() if v is not None}
        return condition,data