from fastapi import Depends, APIRouter, Body , Query, Path
from .model import InsertSchema, GetScheama, GetScheamaAll, UpdateScheama
from fastapi.encoders import jsonable_encoder
import json
import asyncio
from bson.objectid import ObjectId
from datetime import datetime
from ...setting.responseModle import ResponseModel, ResponseCreateModel, ErrorResponseModel
from ...database import mgo_db_connector as database
from fastapi.security import HTTPBearer
from fastapi_jwt_auth import AuthJWT
router = APIRouter()

collection = database.get_collection("history_cauhoi")

try:
    
    collection.create_index('user_id',unique = False)
    collection.create_index('ma_cau_hoi',unique = False)
    collection.create_index('input_testcase',unique = False)
    collection.create_index('output_testcase',unique = False)
    collection.create_index('output',unique = False)
    collection.create_index('console_output',unique = False)
    collection.create_index('Error_output',unique = False)
    collection.create_index('hoan_thanh',unique = False)
    
except Exception as error:
    print(error)

async def check_valid_condition(condition):
    if condition == None:
        return True 
    return await collection.find_one(condition)


async def check_valid(condition):
    return await collection.find_one(condition) != None



@router.post("/", dependencies = [Depends(HTTPBearer())])
async def insert(Authorize:AuthJWT = Depends(), payload: InsertSchema = Body(...)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    print(current_user['id'])
    data = payload.get_params(current_user['id'])

    result = await collection.insert_one(data)
    return str(result.inserted_id)


@router.get("/", dependencies = [Depends(HTTPBearer())])
async def get(payload: GetScheama = Depends()):
    condition = payload.get_params()
    result = collection.find(condition)

    return await payload.format_mgo(result)


@router.get("/All", dependencies = [Depends(HTTPBearer())])
async def get(payload: GetScheamaAll = Depends()):
    condition = payload.get_params()
    result = collection.find(condition)

    return await payload.format_mgo(result)


@router.put("/{id}", dependencies = [Depends(HTTPBearer())])
async def update(id:str=Path(...), payload: UpdateScheama=Body(...)):

    condition, data = payload.get_params(id)
    if not await check_valid_condition(condition):
        return ErrorResponseModel(405,f"Lỗi {id} không tồn tại")

    result = {}
    result = await collection.update_one(condition, {"$set": data})
    print(result)
    return True



@router.delete("/{id}", dependencies = [Depends(HTTPBearer())])
async def delete(id:str=Path(...)):
    condition = {'_id':ObjectId(id)}
    if await collection.find_one(condition) == None:
        return ErrorResponseModel(405,f"lịch sử không tồn tại")
        
    await collection.delete_one(condition)
    return ResponseModel("Thành công","lịch sử")
