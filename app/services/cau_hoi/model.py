from typing import Optional, Dict, List

from bson.objectid import ObjectId
from fastapi import Depends
from pydantic import BaseModel,EmailStr,Field
import time

import re

from ...database import mgo_db_connector as database

collection = database.get_collection("ky_thi")
collection = database.get_collection("cau_hoi")

class InsertSchema(BaseModel):
    ten_cau_hoi:str = Field(...)
    mo_ta:str = Field(...)
    ten_ham: str = Field(...)
    gioi_han_thuc_thi:float = Field(...)
    dau_vao:List[Dict[str,str]] = Field(...)
    kieu_du_lieu_dau_ra:str = Field(...)
    ma_ky_thi:str = Field(...)
    ten_ky_thi:str = Field(...)
    trang_thai:bool = True



class GetSchema(BaseModel):
    ma_ky_thi : str = Field(None)
    ten_cau_hoi : str = Field(None)
    def get_params(self):
        condition= {}
        if self.ma_ky_thi != None:
            condition ["ma_ky_thi"] = self.ma_ky_thi
        if self.ten_cau_hoi != None:
            condition ["ten_cau_hoi"] = self.ten_cau_hoi
        return condition

    async def format_mgo(self, data):
        result = []
        async for r in data:
            r['_id'] = str(r['_id'])
            result.append(r)
        return result


class GetSchemaAll(BaseModel):
    def get_params(self):
        condition= {}

    async def format_mgo(self, data):
        result = []
        async for r in data:
            r['_id'] = str(r['_id'])
            result.append(r)
        return result

class UpdateScheam(BaseModel):
    ten_cau_hoi:Optional[str] = Field(None)
    mo_ta: Optional[str] = Field(None)
    ten_ham: Optional[str]= Field(None)
    gioi_han_thuc_thi:Optional[float]= Field(None)
    dau_vao: Optional[List[Dict[str,str]]]= Field(None)
    kieu_du_lieu_dau_ra: Optional[str]= Field(None)
    

    def get_params(self, id):
        data = {}
        condition = {
            '_id' : ObjectId(id)
        }
        data = {k:v for k,v in self.dict().items() if v is not None}

        return condition, data
 
