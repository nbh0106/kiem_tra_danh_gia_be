#format of testcase in db 
# {
#     'func_name': 'sum',
#     "params": [
#         {
#             "ten_dau_vao": "a",
#             "kieu_du_lieu": "array[integer]",
#         },
#         {
#             "ten_dau_vao": "n",
#             "kieu_du_lieu": "integer",
#         },

#     ],
#     "function": "\
#             int sum(std::vector<int> a,int n)\
#         \n{int sum = 0;\
#         \n for (int i=0;i<n;++i)\
#         \n{\
#         \n    sum+=a[i];\
#         \n}\
#             return sum;}           \
#                        ",
#     "testcase_list": [
#         {
#             "id": "1",
#             "input": [
#                 {
#                     "ten_dau_vao": "a",
#                     "kieu_du_lieu": "array[integer]",
#                     "gia_tri": "[1,2,3,4,5]"
#                 },
#                 {
#                     "ten_dau_vao": "n",
#                     "kieu_du_lieu": "integer",
#                     "gia_tri": "5"
#                 }
#             ],
#             "output": 15,
#             "hidden": False,
#         },

#         {
#             "id": "1",
#             "input": [
#                 {
#                     "ten_dau_vao": "a",
#                     "kieu_du_lieu": "array[integer]",
#                     "gia_tri": "[1,1,1,1,1]"
#                 },
#                 {
#                     "ten_dau_vao": "n",
#                     "kieu_du_lieu": "integer",
#                     "gia_tri": "5"
#                 }
#             ],
#             "output": 5,
#             "hidden": False,
#         }

#     ]
# }

class Language_Generic:
    
    def __init__(self,func_name:str,params:list,function_scripts:str,testcase_list:list) -> None:
        self.func_name = func_name
        self.params = params
        self.function_scripts = function_scripts
        self.testcase_list = testcase_list
    #return file name
    async def generate_build_file(self)->str:
        pass

    async def run_testcase(self):
        pass
