from .language_generic import Language_Generic
from bson.objectid import ObjectId
import aiofiles
import ast,os,traceback,subprocess
import concurrent.futures


class Python(Language_Generic):

    def __init__(self, func_name: str, params: list, function_scripts: str, testcase_list: list) -> None:
        super().__init__(func_name, params, function_scripts, testcase_list)

    # for python do not need to gen build file just write file to folder src/python
    async def generate_build_file(self) -> str:
        file_name = str(ObjectId())
        async with aiofiles.open(f'src/python/{file_name}.py', mode='w') as f:
                await f.write(self.function_scripts)
        return file_name

    async def get_result(self):
        new_testcase_list = []
        # get file name of source code of user
        sourcecode_file_name = await self.generate_build_file()
        # import from module sourcecode_file_name
        module = __import__(
            f'src.python.{sourcecode_file_name}', fromlist=[None])
        # get function from that module
        func_name = getattr(module, self.func_name)
        # run testcase list
        for testcase in self.testcase_list:
        # format of testcase in DB in file language_generic.py
            testcase_id = {'id': testcase['id']}
            testcase_input = {'input': {ip['ten_dau_vao']: ast.literal_eval(
                ip['gia_tri']) for ip in testcase['input']}}
            testcase_output = {'output': ast.literal_eval(testcase['output'])}
            new_testcase_list.append(
                {**testcase_id, **testcase_input, **testcase_output, 'hidden': testcase['hidden']})

        data_returned = []

        with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:

                future_to_url = {executor.submit(func_name, **testcase['input']): testcase for testcase in new_testcase_list}

                for future in concurrent.futures.as_completed(future_to_url):

                        testcase = future_to_url[future]

                        exp = future.exception()
                        

                        r = future.result() if exp == None else None

                        score = 1 if r == testcase['output'] else 0

                        data_returned.append({'id': testcase['id'], 
                                            "expected_ouput": testcase["output"],
                                            "output": r,
                                            "score": score,
                                            "console_log": str(exp),
                                            "hidden": testcase["hidden"]})
        #when finished program then remove that file                                  
        remove_source_code_cmd = f'rm src/python/{sourcecode_file_name}.py'
        subprocess.Popen([remove_source_code_cmd],shell = True)
        
        return data_returned

    async def run_testcase(self):
        try:
            result = await self.get_result()
            return result

        except Exception as exp:
            exp_detail = traceback.format_exc()
            return {'is_compile_error':True,'detail': exp_detail}

        finally:
            pass

