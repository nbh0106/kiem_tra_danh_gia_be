
import json
from typing import List, Optional
from fastapi import APIRouter, Body , Query, Depends, Path
from fastapi.encoders import jsonable_encoder
import time
from bson.objectid import ObjectId

from datetime import datetime
from ...setting.responseModle import ErrorResponseModel, ResponseModel, ResponseCreateModel
from .model import InsertSchema, GetSchema, UpdateScheam, GetSchemaAll
from fastapi.security import HTTPBearer
from enum import Enum
from .helper import CodeTemplate

router = APIRouter()

from ...database import mgo_db_connector as database

collection = database.get_collection("cau_hoi")
try:
    collection.create_index('ten_cau_hoi',unique = False)
    collection.create_index('mo_ta',unique = False)
    collection.create_index('gioi_han_thuc_thi',unique = False)
    collection.create_index('tham_so_dau_vao',unique = False)
    collection.create_index('ket_qua',unique = False)
    collection.create_index('ten_ky_thi',unique = False)
except Exception as error:
    print(error)

test_case_collection = database.get_collection("test_case")


class LanguageType(Enum):
    cpp = 'cpp'
    java = 'java'
    python = 'python'


async def check_valid_condition(condition):
    if condition == None:
        return True
    return await collection.find_one(condition) != None

async def check_valid_conditions(condition):
    return await collection.find_one(condition) != None

@router.post("/", dependencies = [Depends(HTTPBearer())])
async def insert(payload: InsertSchema = Body(...)):
    
    ngay_tao = time.time()
    
    data = {**vars(payload),'ngay_tao':ngay_tao}
    result = await collection.insert_one(data)

    return str(result.inserted_id)

@router.get("/kythi/{ky_thi_id}", dependencies = [Depends(HTTPBearer())])
async def get_via_kythi(ky_thi_id:str = Path(...)):
    cau_hois_db = collection.find({'ma_ky_thi':ky_thi_id,'trang_thai':True})
    cau_hois_returned = []
    async for cau_hoi in cau_hois_db:
        cau_hoi['id'] = str(cau_hoi['_id'])
        del cau_hoi['_id']
        cau_hois_returned.append(cau_hoi)
    
    return cau_hois_returned

@router.get("/", dependencies = [Depends(HTTPBearer())])
async def get(payload: GetSchema = Depends()):
    condition = payload.get_params()

    result = collection.find(condition)
    return await payload.format_mgo(result)


@router.get("/GetAll", dependencies = [Depends(HTTPBearer())])
async def get(payload: GetSchemaAll = Depends()):
    condition = payload.get_params()

    result = collection.find(condition)
    return await payload.format_mgo(result)

@router.get("/{id}/function_template", dependencies = [Depends(HTTPBearer())])
async def get_fucntion_template(id:str=Path(...), language: LanguageType = Query(...)):
    
    cau_hoi = await collection.find_one({'_id': ObjectId(id)})
    
    template = CodeTemplate(language_pro_name=language.value,func_name=cau_hoi['ten_ham'],params=cau_hoi['dau_vao'],output_type=cau_hoi['kieu_du_lieu_dau_ra'])
    
    return str(template)

@router.post("/{id}/run_test")
async def run_test(id:str=Path(...), language: LanguageType = Query(...), code:str = Body(...)):
    
    
    # Get all testcase of question
    payload = {}
    cau_hoi = await collection.find_one({'_id': ObjectId(id)})
    # Get Infor
    payload.update({"func_name":cau_hoi["ten_ham"],'params':cau_hoi["dau_vao"],'function_scripts':code})
    

    # Get testcase
    test_case_list =  test_case_collection.find({"ma_cau_hoi": id})
    input_testcase = []
    async for test_case in  test_case_list: 
        input_testcase.append({
            "id": str(test_case["_id"]),
            "hidden": test_case["an"], 
            "output": test_case["gia_tri_dau_ra"]["gia_tri"], 
            "input": test_case["chuoi_gia_tri_dau_vao"],
        })
    payload["testcase_list"] = input_testcase
    language_module = __import__(f'app.services.cau_hoi.kiem_tra_dau_vao.coding.{language.value}',fromlist=[None])
    
    #make generic funtion
    #example for make generic function is following comment
    async def run_test_generic(language_name:str):
        Language_Module = getattr(language_module,language_name.title())
        language_object = Language_Module(**payload)
        data_returned = await language_object.run_testcase()

        return data_returned
     
    
    # if language.value == 'cpp':
    #     Cpp = getattr(language_module,'Cpp')
    #     cpp = Cpp(payload['func_name'],payload['params'],payload['function_scripts'],payload['testcase_list'])
    #     data_returned = await cpp.run_testcase() 
        
    #     return data_returned
    # if language.value == 'python':
    #     Python = getattr(language_module,'Python')
    #     python = Python(payload['func_name'],payload['params'],payload['function_scripts'],payload['testcase_list'])
    #     data_returned = await python.run_testcase() 
    #     print(data_returned)
    #     return data_returned
    
    return await run_test_generic(language_name=language.value)

@router.put("/{id}", dependencies = [Depends(HTTPBearer())])
async def update(id:str=Path(...), payload: UpdateScheam = Body(...)):
    
    condition ,data = payload.get_params(id)
    if not await check_valid_conditions(condition):
        return ErrorResponseModel(405,"Câu hỏi không tồn tại")
    
    result = {}
    result = await collection.update_one(condition, {'$set': data})
    return True

@router.delete("/{id}", dependencies = [Depends(HTTPBearer())])
async def delete(id:str=Path(...), force:bool=Query(False)):
    condition = {'_id':ObjectId(id)}
    if await collection.find_one(condition) == None:
        return ErrorResponseModel(404,"Câu hỏi chưa tồn tại")
    if force == True:
        await collection.delete_one(condition)
    else:
        await collection.update_one(condition, {'$set':{'trang_thai':False}})
    return ResponseModel("Thành công","Đã xóa câu hỏi")