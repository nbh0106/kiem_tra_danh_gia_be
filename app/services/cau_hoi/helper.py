# #cpp
# def get_temp_code_cpp(func_name, output_data_type, params):
#     param_inputs=[]
#     for param in params:
#         if param["kieu_du_lieu"] == 'integer':
#             data_type_name = "int"
#         elif param["kieu_du_lieu"] == 'float':
#             data_type_name = "float"
#         elif param["kieu_du_lieu"] == 'string':
#             data_type_name = "string"
#         elif param["kieu_du_lieu"] == 'boolean':
#             data_type_name = "bool"
#         elif param["kieu_du_lieu"] == 'array[integer]':
#             data_type_name = "*int"
#         elif param["kieu_du_lieu"] == 'array[float]':
#             data_type_name = "*float"
#         elif param["kieu_du_lieu"] == 'array[string]':
#             data_type_name = "*string"
#         elif param["kieu_du_lieu"] == 'array[boolean]':
#             data_type_name = "*bool"

#         param_inputs.append(data_type_name + ' ' + param["ten_dau_vao"])
    
#     r_default_value = 0
#     r_output_data_type = "int"
#     if output_data_type == 'integer':
#         r_output_data_type = "int"
#     elif output_data_type == 'float':
#         r_output_data_type = "float"
#         r_default_value = 0.0
#     elif output_data_type == 'string':
#         r_output_data_type = "string"
#         r_default_value = ""
#     elif output_data_type == 'boolean':
#         r_output_data_type = "bool"
#         r_default_value = False

#     params_type = ', '.join(param_inputs)
#     temp = '{0} {1}({2}) {{ \n\treturn {3}; \n}}'.format(r_output_data_type, func_name, params_type, r_default_value)
#     return temp

# #java
# def get_temp_code_java(func_name, output_data_type, params):
#     param_inputs=[]
#     for param in params:
#         if param["kieu_du_lieu"] == 'integer':
#             data_type_name = "int"
#         elif param["kieu_du_lieu"] == 'float':
#             data_type_name = "float"
#         elif param["kieu_du_lieu"] == 'string':
#             data_type_name = "String"
#         elif param["kieu_du_lieu"] == 'boolean':
#             data_type_name = "boolean"
#         elif param["kieu_du_lieu"] == 'array[integer]':
#             data_type_name = "int[]"
#         elif param["kieu_du_lieu"] == 'array[float]':
#             data_type_name = "float[]"
#         elif param["kieu_du_lieu"] == 'array[string]':
#             data_type_name = "String[]"
#         elif param["kieu_du_lieu"] == 'array[boolean]':
#             data_type_name = "boolean[]"

#         param_inputs.append(data_type_name + ' ' + param["ten_dau_vao"])
    
#     r_default_value = 0
#     r_output_data_type = 'int'
#     if output_data_type == 'integer':
#         r_output_data_type = "int"
#     elif output_data_type == 'float':
#         r_output_data_type = "float"
#         r_default_value = 0.0
#     elif output_data_type == 'string':
#         r_output_data_type = "string"
#         r_default_value = ""
#     elif output_data_type == 'boolean':
#         r_output_data_type = "boolean"
#         r_default_value = False
    
#     params_type = ', '.join(param_inputs)
#     temp = '{0} {1}({2}) {{ \n\treturn {3}; \n}}'.format(r_output_data_type, func_name, params_type, r_default_value)
#     return temp

# #python
# def get_temp_code_python(func_name, output_data_type, params):
#     param_inputs=[]
#     for param in params:        
#         param_inputs.append(param["ten_dau_vao"])
#     params_type = ', '.join(param_inputs)
#     temp = '{0} {1}({2}):\n\treturn \n'.format('def', func_name, params_type)
#     return temp

# if __name__ == "__main__":
#     temp_code = get_temp_code_cpp('sum', 'integer', [{"ten_dau_vao" : 'a',"kieu_du_lieu": 'integer'}, 
#         {"ten_dau_vao": 'b', "kieu_du_lieu": 'integer'}])
#     print(temp_code)
#     temp_code = get_temp_code_java('sum', 'integer', [{"ten_dau_vao" : 'a',"kieu_du_lieu": 'string'}, 
#         {"ten_dau_vao": 'b', "kieu_du_lieu": 'integer'}])
#     print(temp_code)
#     temp_code = get_temp_code_python('sum', 'integer', [{"ten_dau_vao" : 'a',"kieu_du_lieu": 'integer'}, 
#         {"ten_dau_vao": 'b',"kieu_du_lieu": 'integer'}])
#     print(temp_code)

# format of cau hoi in mongo db 
# cau_hoi = {
#     "_id": "611a4030d96231b22d8e3f31",
#     "ten_cau_hoi": "Tính tổng của một chuỗi số nguyên",
#     "mo_ta": "cho chuỗi số nguyên a =[1,2,3,4] và kết quả tong(a)=10",
#     "ten_ham": "tong",
#     "gioi_han_thuc_thi": 10,
#     "dau_vao": [
#         {
#             "ten_dau_vao": "a",
#             "kieu_du_lieu": "Array[integer]"
#         },
#         {
#             "ten_dau_vao": "b",
#             "kieu_du_lieu": "string"
#         }
#     ],
#     "kieu_du_lieu_dau_ra": "integer"

# }

from .constaint_data import LANGUAGE_SYNTAX_DATA
class CodeTemplate:
    
    def __init__(self, language_pro_name: str, func_name: str, params: list, output_type: str) -> None:
        self.language_pro_name = language_pro_name.strip().lower()
        self.func_name = func_name.strip()
        self.params = params
        self.output_type = output_type.strip().lower()

    def __str__(self) -> str:
        params_string = ','.join((f'{LANGUAGE_SYNTAX_DATA[self.language_pro_name]["data_type"][param["kieu_du_lieu"].strip().lower()]} {param["ten_dau_vao"]}' for param in self.params)) if self.language_pro_name in {
            'cpp', 'java'} else ','.join((f'{param["ten_dau_vao"]}:{LANGUAGE_SYNTAX_DATA[self.language_pro_name]["data_type"][param["kieu_du_lieu"].strip().lower()]}' for param in self.params))
        
        data_format = {'output_type':LANGUAGE_SYNTAX_DATA[self.language_pro_name]['data_type'][self.output_type],'func_name':self.func_name,'params':params_string}
        
        template =  LANGUAGE_SYNTAX_DATA[self.language_pro_name]['format'].format(**data_format)

        if ('string' in (dv['kieu_du_lieu'] for dv in self.params) or self.output_type == 'string') and self.language_pro_name == 'cpp':
            template = '#include <string>\n\n'+template
        
        return template



