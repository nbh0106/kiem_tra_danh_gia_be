from typing import Collection
from typing import Optional,Dict
from bson.objectid import ObjectId

from fastapi import Depends
import time
from ...database import mgo_db_connector as database
from pydantic import BaseModel,EmailStr,Field

Collection = database.get_collection("ky_thi")


class InsertSchema(BaseModel):
    ten_ky_thi: str = Field(...)
    tg_bat_dau: int = Field(...)
    tg_ket_thuc: int = Field(...)
    # ngay_tao:time = Field(...)
    thoi_gian_lam_bai: int = Field(...)
    ma_course:int = Field(...)
    trang_thai:bool = True


    def get_params(self):


        if(self.tg_bat_dau =="" or self.tg_ket_thuc ==""  or self.ten_ky_thi == "" or self.ma_course == ""):
            condition = None
        else:
            condition={
                "ten_ky_thi":self.ten_ky_thi
            }
        data ={
            'ten_ky_thi': self.ten_ky_thi,
            'tg_bat_dau': self.tg_bat_dau,
            'tg_ket_thuc': self.tg_ket_thuc,
            'ngay_tao':int(time.time()),
            # 'anh_dai_dien':self.anh_dai_dien,
            'thoi_gian_lam_bai':self.thoi_gian_lam_bai,
            'ma_course':self.ma_course,
            'trang_thai':self.trang_thai,
        }
        print(data)

        return condition,data

class GetScheama(BaseModel):
    ten_ky_thi: str = Field(...)
    ma_course:str = Field(...)
    def get_params(self):
        condition={}

        if self.ten_ky_thi != None:
            condition['ten_ky_thi'] = self.ten_ky_thi

        if self.ma_course != None:
            condition['ma_course'] = self.ma_course

        return condition

    async def format_mgo(self,data):
        result = []

        async for r in data:
            r['_id'] = str(r['_id'])

            result.append(r)
        return result

class GetScheamaAll(BaseModel):
    def get_params(self):
        
        condition={}

    async def format_mgo(self,data):
        result = []

        async for r in data:
            r['_id'] = str(r['_id'])

            result.append(r)
        return result

class UpdateScheama(BaseModel):
    ten_ky_thi: Optional[str]
    tg_bat_dau: Optional[int]
    tg_ket_thuc: Optional[int]
    # anh_dai_dien: Optional[str]
    
    thoi_gian_lam_bai: Optional[int]


    def get_params(self,id):
        data = {}
        condition ={
            "_id":ObjectId(id)
        }

        data={k:v for k,v in self.dict().items() if v is not None}
        return condition,data