from fastapi import Depends, APIRouter, Body , Query, Path
from .model import InsertSchema, GetScheama, UpdateScheama, GetScheamaAll
from fastapi.encoders import jsonable_encoder
import json
import asyncio
from bson.objectid import ObjectId
from datetime import datetime
from ...setting.responseModle import ResponseModel, ResponseCreateModel, ErrorResponseModel
from ...database import mgo_db_connector as database
from fastapi.security import HTTPBearer
router = APIRouter()

collection = database.get_collection("ky_thi")

try:
    
    collection.create_index('ten_ky_thi',unique = False)
    collection.create_index('tg_bat_dau',unique = False)
    collection.create_index('tg_ket_thuc',unique = False)
    collection.create_index('ngay_tao',unique = False)
    
except Exception as error:
    print(error)


async def check_valid_condition(condition):
    if condition == None:
        return True 
    return await collection.find_one(condition)


async def check_valid(condition):
    return await collection.find_one(condition) != None



@router.post("/", dependencies = [Depends(HTTPBearer())])
async def insert(payload: InsertSchema=Body(...)):

    condition, data = payload.get_params()    
    
    if condition is None:
        return ErrorResponseModel(405,f"Tên hoặc TGBĐ hoặc TGKT hoặc ảnh kỳ thi bị trống")
    
    if await check_valid(condition):
        return ErrorResponseModel(405,f"Tên bị trùng")
    
    result = await collection.insert_one(data)
    
    return str(result.inserted_id)



@router.get("/", dependencies = [Depends(HTTPBearer())])
async def get(payload: GetScheama = Depends()):
    condition = payload.get_params()
    result = collection.find(condition)

    return await payload.format_mgo(result)


@router.get("/GetAll", dependencies = [Depends(HTTPBearer())])
async def get(payload: GetScheamaAll = Depends()):
    condition = payload.get_params()
    result = collection.find(condition)

    return await payload.format_mgo(result)

@router.get("/GetAllViaCourse/{course_id}", dependencies = [Depends(HTTPBearer())])
async def get(payload: GetScheamaAll = Depends(),course_id:int = Query(...)):
    list_ky_thi_db =  collection.find({'ma_course':course_id,'trang_thai':True})
    list_ky_thi = []
    async for kt in list_ky_thi_db:
        kt['id']=str(kt['_id'])
        del kt['_id']
        list_ky_thi.append(kt)
    return list_ky_thi
        




@router.put("/{id}", dependencies = [Depends(HTTPBearer())])
async def update(id:str=Path(...), payload: UpdateScheama=Body(...)):

    condition, data = payload.get_params(id)
    if not await check_valid_condition(condition):
        return ErrorResponseModel(405,f"Lỗi {id} không tồn tại")

    result = {}
    result = await collection.update_one(condition, {"$set": data})
    print(result)
    return True



@router.delete("/{id}", dependencies = [Depends(HTTPBearer())])
async def delete(id:str=Path(...)):
    condition = {'_id':ObjectId(id)}
    if await collection.find_one(condition) == None:
        return ErrorResponseModel(405,f"Kỳ thi không tồn tại")
    else:
        await collection.update_one(condition,{'$set':{'trang_thai':False}})

    return ResponseModel("Thành công","Đã xóa kỳ thi")
