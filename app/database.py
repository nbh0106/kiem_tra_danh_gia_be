import mysql.connector

#
from mysql.connector import pooling
import motor.motor_asyncio


connection_pool = pooling.MySQLConnectionPool(pool_name="exams_pool",
                                              pool_size=32,
                                              pool_reset_session=False,
                                              host="127.0.0.1",
                                              user="root",
                                              password="123456789",
                                              database="moodle",
                                              port=3307,
                                              
)


MONGO_DETAILS = 'mongodb://localhost:27017/'
client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)
mgo_db_connector = client.moodle

#connect mysql
#start = time.time()

mydb = mysql.connector.connect(
  host="127.0.0.1",
  user="root",
  password="123456789",
  database="moodle",
  port=3307
)

mydb.autocommit = True
#print('Connect Time', time.time() - start)