
import asyncio
import json
import subprocess
import aiofiles
import motor.motor_asyncio


MONGO_DETAILS = 'mongodb://localhost:27017/'
client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)
mgo_db_connector = client.moodle

db = mgo_db_connector.get_collection('coding_exam_history')

async def read_file_html(file_path:str) ->str:
    async with aiofiles.open(file_path,mode='r') as f:
        await f.seek(0)
        contents = await f.read()
        return contents


async def grade_assign(assign):
    quiz_name = assign['quiz_name']
    course_name = assign['course_name']
    user_id = assign['user_id']

    # cmd_list=[]
    QUIZ_SOURCE = f'/data/owncloud/exams/Assiment/source/{quiz_name}'
    # QUIZ_SOURCE = f'/home/vnas/source/Assiment/source/{quiz_name}'
    # CMD = f'cp -r \'{QUIZ_RELEASE_PATH}\' \'{QUIZ_SOURCE}\''

    # p = subprocess.Popen([CMD],stdout=subprocess.PIPE, shell=True)
    # t1 = p.wait()

    # cmd_list.append({'is_terminated':True if t1 ==0 else False,'cmd':p})

    # print(CMD)
    # proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True) 
    # t2 = proc.wait()


    # cmd_list.append({'is_terminated':True if t2==0 else False,'cmd':proc})

    # for cmd in cmd_list:
    #     if cmd.get('is_terminated'):
    #         cmd.get('cmd').terminate()

    # gen file to html and save to db
    # file_source_html =await gen_file_to_html(path=QUIZ_SOURCE)
        
    # record = await mgo_db_connector.solutions.find_one({'quiz_id':quiz_id})
    # if not record:
    #     record = {'quiz_id':quiz_id,'course_name':course_name,'quiz_name':quiz_name,'solution':contents_file_source_html,'time_close':time_close}
    #     await mgo_db_connector.solutions.insert_one(record)
        
    QUIZ_SOURCE_DOCKER = f'/home/jovyan/exams/source/ps1'

    QUIZ_SUBMIT_PATH = f'/data/owncloud/exams/code-exams/{course_name}/submitted/{user_id}/{quiz_name}'    
    
    QUIZ_SUBMIT_PATH_DOCKER = f'/home/jovyan/exams/submitted/user/ps1'
    

    CMD = 'docker run -it --rm -v \'{}\':\'{}\' -v \'{}\':\'{}\' grader-jupyter:latest'.format(
        QUIZ_SOURCE,
        QUIZ_SOURCE_DOCKER,
        QUIZ_SUBMIT_PATH,
        QUIZ_SUBMIT_PATH_DOCKER,
    )

    print(CMD)
    
    #proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True)
    #proc.wait()

    import os
    stream = os.popen(CMD)
    msg = stream.read()

    contents_file_source_html = 'Bài tập của bạn bị lỗi. Vui lòng liên hệ quản lý lớp học để giải quyết.'
    result_dict = {'max_score': 10.0, 'score': 0.0}
    result = ''

    try: 
        # msg = str(stream.communicate()[0].decode('utf-8'))
        result = str(msg.splitlines()[-1].strip()).replace("'",'"')
        print(result)
        #must replace ' to " because json only get " not ' when json.loads
        score = json.loads(result)
        result_dict['score'] = score['score']
        print(result_dict) 
        file_source_html = f'{QUIZ_SUBMIT_PATH}/{quiz_name}.html'
        contents_file_source_html = await read_file_html(file_source_html)
    except Exception as error:
        print(result)
        # contents_file_source_html = f'{contents_file_source_html}\n{result}'
        print(error)

    data = {}
    data.update({
        'my_answers':contents_file_source_html,
        'is_graded': True,
    })
    data.update(result_dict)

    print(data)
    await db.update_one({'_id': assign['_id']}, {'$set': data})


async def get_assignment_list():
    submitted =  db.find({'is_graded': False})
        
    for assign in await submitted.to_list(length=10):
        print(assign)
        await grade_assign(assign)


import time
loop = asyncio.get_event_loop()
while True:
    loop.run_until_complete(get_assignment_list())
    time.sleep(1)





    # user_id=str(user_id)
    
    # QUIZ_SOURCE = f'/home/ds1/sonnm/Assiment/source/{quiz_name}'
    # #gen file to html and save to db
    # # file_source_html =await gen_file_to_html(path=QUIZ_SOURCE)
    
    # # contents_file_source_html = await read_file_html(file_source_html)
        
    # # record = await mgo_db_connector.solutions.find_one({'quiz_id':quiz_id})
    # # if not record:
    # #     record = {'quiz_id':quiz_id,'course_name':course_name,'quiz_name':quiz_name,'solution':contents_file_source_html,'time_close':time_close}
    # #     await mgo_db_connector.solutions.insert_one(record)
        
    # QUIZ_SOURCE_DOCKER = f'/home/jovyan/exams/source/ps1'

    # QUIZ_SUBMIT_PATH = f'/home/ds1/sonnm/AI/data/code-exams/{course_name}/submitted/{user_id}/{quiz_name}'
    # #gen file to html and save to db
    # # my_file_html =await gen_file_to_html(path=QUIZ_SUBMIT_PATH)
    
    # # contents_my_file_html = await read_file_html(my_file_html)
        
    # # data.update({'my_answers':contents_my_file_html})
    
    
    # QUIZ_SUBMIT_PATH_DOCKER = f'/home/jovyan/exams/submitted/user/ps1'
    

    # CMD = 'docker run -it --rm -v \'{}\':\'{}\' -v \'{}\':\'{}\' grader-jupyter:latest'.format(
    #     QUIZ_SOURCE,
    #     QUIZ_SOURCE_DOCKER,
    #     QUIZ_SUBMIT_PATH,
    #     QUIZ_SUBMIT_PATH_DOCKER,
    # )

    # print(CMD)
    
    # proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True)

    # msg = str(proc.communicate()[0].decode('utf-8'))

    # result = str(msg.splitlines()[-1].strip()).replace("'",'"')
    # #must replace ' to " because json only get " not ' when json.loads
    
    # result_dict = json.loads(result)
    
    # data.update(result_dict)
