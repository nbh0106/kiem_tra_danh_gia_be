


# def swagger_monkey_patch(*args, **kwargs):
#     """
#     Wrap the function which is generating the HTML for the /docs endpoint and 
#     overwrite the default values for the swagger js and css.
#     """
#     return get_swagger_ui_html(
#         *args, **kwargs,
#         swagger_js_url="/root/Vingroup/auth-api/swagger-ui/dist/swagger-ui-bundle.js",
#         swagger_css_url="/root/Vingroup/auth-api/swagger-ui/dist/swagger-ui.css")

# # Actual monkey patch
# applications.get_swagger_ui_html = swagger_monkey_patch

# try:
    #from __future__ import absolute_import
from fastapi import applications
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi import FastAPI, HTTPException, Depends, Request,Body
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic import BaseModel,Field
from fastapi.security import OAuth2PasswordBearer
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
from fastapi.security import HTTPBearer
from fastapi.openapi.utils import get_openapi
from fastapi.middleware.cors import CORSMiddleware
from typing import Optional

from app.auth import check_auth, get_user, get_user_role,save_info_mongo
import time 
import app.configs as cf
import pretty_errors
import json

# import routers
from app.server.quiz.route import router as QuizRouter
from app.server.course.route import router as CourseRouter
from app.server.coding_exams.route import router as CodingRouter
from app.services.ky_thi.route import router as kythi
from app.services.cau_hoi.route import router as cauhoi
from app.services.test_case.route import router as testcase
from app.services.history_cauhoi.route import router as historycauhoi
from app.database import mgo_db_connector
from notebook.auth import passwd

# except Exception as exp:
#     print("Some modules is not valid")
#     print(exp)

# In the real case, you can put the
# public key and private key in *.pem, *.key then you can read that file

#config pretty errors


private_key = """
-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgGBoQhqHdMU65aSBQVC/u9a6HMfKA927aZOk7HA/kXuA5UU4Sl+U
C9WjDhMQFk1PpqAjZdCqx9ajolTYnIfeaVHcLNpJQ6QXLnUyMnfwPmwYQ2rkuy5w
I2NdO81CzJ/9S8MsPyMl2/CF9ZxM03eleE8RKFwXCxZ/IoiqN4jVNjSrAgMBAAEC
gYAnNqEUq146zx8TT6PilWpxB9inByuVaCKkdGPbsG+bfa1D/4Z44/4AUsdpx5Ra
s/hBkMRcIOsSChMAUe8xcK0DqA9Y7BIVfpma2fH/gYq6dP3dOfCxftZBF00HwIu7
5e7RWnBC8MkPnrkKdHq6ptAYlGgoSJTEQREqusDiuNG9yQJBAKQib2VhNAqgyvvi
PdmFrCqq15z9MY16WCfttuqfAaSYKHnZe1WvBKbSNW9x4Cgjfhzl9mlozlW4rob/
ttPN6e0CQQCWXbVtqmVdB5Ol9wQN7DIRc8q5F8HKQqIJAMTmwaRwNDsGRxCWMwGO
8WAlnejzYTXmrrytv6kXX8U40enJW2X3AkAI42h+5/WmgbCcVVMeHXQGV3wXn0p4
q+BsQR4/tF6laCwA9TsNl827rvR/1X3bDpj8vaNLcAaEc9zXqK9g5uy9AkATeOkw
3Xso8/075eRBhU/qkKs1Ew2GiuB+9/mHxJXt7eWi53sPaGWQRFPmKy/qrLEVQZWv
jn1wSHe65vw2lj57AkEAh04n1wrZnCha8s6crMhjggdTXI6G4FU3TGf8ssGboqs3
j5lemvyKod+u2JVKwarcKmd/gFYBOjsRm18LlZH74A==
-----END RSA PRIVATE KEY-----
"""

public_key = """
-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgGBoQhqHdMU65aSBQVC/u9a6HMfK
A927aZOk7HA/kXuA5UU4Sl+UC9WjDhMQFk1PpqAjZdCqx9ajolTYnIfeaVHcLNpJ
Q6QXLnUyMnfwPmwYQ2rkuy5wI2NdO81CzJ/9S8MsPyMl2/CF9ZxM03eleE8RKFwX
CxZ/IoiqN4jVNjSrAgMBAAE=
-----END PUBLIC KEY-----
"""

app = FastAPI(
        title="Backend Port: Hệ thống kiểm tra đánh giá AI",
)

origins = [
         
            'http://118.70.52.237:8078',
             'http://118.70.52.237:8079',
            'http://ha-exams.aiacademy.edu.vn:8888',
           'https://ha-exams.aiacademy.edu.vn',
            "http://localhost:4200",
           "http://exams.aiacademy.edu.vn", 
           "https://exams.aiacademy.edu.vn",
           "http://45.77.245.61:8888",
           'https://45.77.245.61',
           'http://ha.aiacademy.edu.vn'
        ]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
#incluse all route
app.include_router(QuizRouter,tags=["Quiz"],prefix="/quiz")
app.include_router(CourseRouter,tags=["Course"],prefix='/course')
app.include_router(CodingRouter,tags=["Coding Exams"],prefix='/coding-exams')
app.include_router(kythi,tags=["Lược đồ kỳ thi"], prefix="/kythi")
app.include_router(cauhoi,tags=["Lược đồ câu hỏi"], prefix="/cauhoi")
app.include_router(testcase,tags=["Lược đồ testcase"], prefix="/testcase")
app.include_router(historycauhoi,tags=["History_cauhoi"], prefix="/history_cauhoi")

def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="AI Academy Vietnam: Hệ thống kiểm tra đánh giá AI",
        version="1.0.0",
        description="Danh sách rest api cho hệ thống kiểm tra đánh giá",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi


class User(BaseModel):
    username: str = Field(cf.USERNAME)
    password: str = Field(cf.PASSWORD)

class Settings(BaseModel):
    authjwt_algorithm: str = "RS512"
    authjwt_public_key: str = public_key
    authjwt_private_key: str = private_key

ROLE= {
    1: 'Manager',
    2: 'Course Creator',
    3: 'Editing Teacher',
    4: 'Teacher',
    5: 'Student',
    6: 'Guest',
    7: 'User',
    8: 'Guest'
}

@AuthJWT.load_config
def get_config():
    return Settings()

@app.exception_handler(AuthJWTException)
async def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.message}
    )

@app.post('/login', tags=["Users"])
async def login(user: User = Body(...), Authorize: AuthJWT = Depends()):
    valid, user_info = await check_auth(user.username, user.password)
    if (not valid):
        raise HTTPException(status_code=401,detail="Bad username or password")
    
    await save_info_mongo(user_info[1], user.username, user.password)
    
    user = {
        'id': user_info[1], 
        'name': f'{user_info[2]} {user_info[3]}', 
        'shortname': user.username.split('@')[0],
        'role': await get_user_role(user_info[1]),
    }
    
    access_token = Authorize.create_access_token(subject= json.dumps(user), expires_time=cf.TOKEN_EXPIRE_TIME)
    #refresh_token = Authorize.create_refresh_token(subject=user_claim, expires_time=60*5)
    
    user.update({'access_token':access_token,"expires_time":cf.TOKEN_EXPIRE_TIME})
    return user


# @app.post('/testRefresh',dependencies=[Depends(HTTPBearer())], tags=["Users"])
# def testRefresh(Authorize: AuthJWT = Depends()):
#     Authorize.jwt_refresh_token_required()    
#     current_user = Authorize.get_jwt_subject()
#     return current_user
    
    
@app.post('/refresh', dependencies=[Depends(HTTPBearer())], tags=["Users"])
async def refresh(Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    #Authorize.jwt_refresh_token_required()

    current_user = json.loads(Authorize.get_jwt_subject())
    
    expires_time=cf.TOKEN_EXPIRE_TIME
    access_token = Authorize.create_access_token(subject=json.dumps(current_user), expires_time=expires_time)
    
    current_user.update({"access_token":access_token, "expires_time":expires_time})
    
    return current_user

    
@app.get('/protected', dependencies=[Depends(HTTPBearer())], tags=["Users"])
async def protected(Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()

    current_user = Authorize.get_jwt_subject()
    return {"user": current_user}





