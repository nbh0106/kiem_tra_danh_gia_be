from ...database import connection_pool
from mysql.connector import Error


# async def get(query:str,is_get_one:bool = True):
#     try:
#         # Get connection object from a pool
#         connection_object = connection_pool.get_connection()

#         if connection_object.is_connected():
#             cursor = connection_object.cursor()
#             cursor.execute(query)

#             record = cursor.fetchone() if is_get_one else cursor.fetchall()
            
#             return record
#     except Error as e:
#         print("Error while connecting to MySQL using Connection pool ", e)
        
#     finally:
#     # closing database connection.
#         if connection_object.is_connected():
#             cursor.close()
#             connection_object.close()
        
             
import asyncio
import aiomysql


# async def get(query:str,is_get_one:bool = True):
#     pool = await aiomysql.create_pool(host='127.0.0.1', port=3307,
#                                     user='root', password='123456789',
#                                     db='moodle')

#     async with pool.acquire() as conn:
#         async with conn.cursor() as cur:
#             await cur.execute(query)
            
#             r = await cur.fetchone() if is_get_one else  await cur.fetchall()
            
#     pool.close()
#     await pool.wait_closed()
#     return r


loop = asyncio.get_event_loop()


async def get(query:str,is_get_one:bool = True):
    conn = await aiomysql.connect(host='127.0.0.1', port=3307,
                                       user='root', password='123456789', db='moodle',
                                       loop=loop)

    cur = await conn.cursor()
    await cur.execute(query)
    
    r = await cur.fetchone() if is_get_one else await cur.fetchall()
    
    await cur.close()
    conn.close()

    return r
        
    
             