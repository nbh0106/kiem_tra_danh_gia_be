try:
    from app.database import mydb, connection_pool
    import pretty_errors
    from app.auth import get_user
    from app.server.course.helper import parse_to_course_model
    from ...server.common_helpers.execute_query_mysql import get as QueryGet
    import time
except Exception as exp:
    print(exp)




async def get_course_of_user(user):
        
    #print(1, time.time() - start)

    userid = user["id"]
    


    query ='''
        select  
            me.id as enrol_id, mc.id as course_id, 
            mc.fullname, mc.shortname,mc.startdate,
            mc.enddate 
        from
            mdl_user_enrolments as mue, 
            mdl_enrol as me, 
            mdl_course as mc
        where
            mue.userid={0}
            and
                mue.enrolid=me.id 
            and 
                me.courseid=mc.id 
    '''.format(userid)
    
    course_list = await QueryGet(query=query,is_get_one=False)
    

    # mydb.commit()
    try:
        course_list = [list(course) for course in course_list]
    except Exception as e:
        print('wrong')
    course_id_list = [str(course[1]) for course in course_list]
    image_list = await get_course_image(course_id_list)

    for i in range(len(course_list)):
        course_id = course_list[i][1]
        if course_id not in image_list:
            course_list[i].append('https://moodle.aiacademy.edu.vn/pluginfile.php/13912/course/overviewfiles/Bigdata.png')
        else:
            course_list[i].append(image_list[course_id])
    
    return list(parse_to_course_model(course)  for course in course_list)
        
    
async def get_course_image(course_id_list):
    query = '''
        SELECT f.id, f.contextid, f.component, f.filearea, f.itemid, f.filepath, f.filename, c.id
        FROM mdl_context cx
        JOIN mdl_course c ON cx.instanceid=c.id
        JOIN mdl_files f ON cx.id=f.contextid
        WHERE f.filename <> '.'
        AND f.component = 'course'
        AND c.id IN ({})'''.format(','.join(course_id_list))
    
    
    #https://moodle.aiacademy.edu.vn/pluginfile.php/13912/course/overviewfiles/Bigdata.png
    image_list = await QueryGet(query=query,is_get_one=False)
    # mydb.commit()
    # mycursor.close()
    
    result = {}
    
    for image in image_list:
        try:
            result[image[7]] = '{}/{}/{}/{}/{}'.format('https://moodle.aiacademy.edu.vn/pluginfile.php', image[1], image[2], image[3], image[6])
            # result.append('{}/{}/{}/{}/{}'.format('https://moodle.aiacademy.edu.vn/pluginfile.php', image[1], image[2], image[3], image[6]))
        except:
            # result.append('https://moodle.aiacademy.edu.vn/pluginfile.php/13912/course/overviewfiles/Bigdata.png')

            pass  
    return result 

async def get_all_course_db():
    query = '''
    select  
            mc.id as course_id, 
            mc.fullname, mc.shortname,mc.startdate,
            mc.enddate 
        from
            
            mdl_course as mc
        
    '''
    course_list = await QueryGet(query=query,is_get_one=False)

    def parse(course):
        return {'course_id':course[0],'fullname':course[1],'shortname':course[2],'startdate':course[3],'enddate':course[4]}


    return [parse(course) for course in course_list]
