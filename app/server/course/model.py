from pydantic import BaseModel,Field
from typing import Optional

class Course(BaseModel):
    course_id:int = Field(...)
    enroll_id:int = Field(...)
    full_name:str = Field(...)
    short_name:str = Field(...)
    start_time:int = Field(...)
    end_time:int = Field(...)
    image_path:str = Field(...)
    

    

