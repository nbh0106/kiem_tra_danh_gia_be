from app.server.course.model import Course

def parse_to_course_model(course):
    return Course(enroll_id = course[0],course_id = course[1],full_name = course[2],short_name=course[3],start_time=course[4],end_time = course[5], image_path = course[6])


