from time import sleep


try:
    import time
    from fastapi import APIRouter,Depends,HTTPException
    from fastapi.security import HTTPBearer
    from fastapi_jwt_auth import AuthJWT
    from app.server.course.database import get_course_of_user,get_all_course_db
    import json
    import asyncio
    
    
    
except Exception as exp:
    print(exp)

router = APIRouter()

@router.get('', dependencies=[Depends(HTTPBearer())])
async def get_courses(Authorize: AuthJWT = Depends()):
    # print('hello')
    #await asyncio.sleep(10)
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    r = await get_course_of_user(user=current_user)

    
    return r

@router.get('/all', dependencies=[Depends(HTTPBearer())])
async def get_all_courses(Authorize: AuthJWT = Depends()):
    # print('hello')
    #await asyncio.sleep(10)
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    #now = time.time()
    #condition = f'and NOT enddate < {now} '
    if current_user['role'] < 5:
        r = await get_all_course_db()
        return r
    else:
        raise   HTTPException(status_code = 403,detail = 'Not Authentication!')
