from os import lockf


try:
    from typing import List
    from subprocess import Popen
    import json
    import socket
    from contextlib import closing
    import threading 
    import pretty_errors
    import asyncio
    import os
    import subprocess
    from app.auth import get_password
    from .helper import gen_file_to_html,read_file_html
    from ...database import mgo_db_connector
    from bson.objectid import ObjectId
    from os import wait

    from pydantic.utils import Obj

    from ...database import mgo_db_connector
    from pathlib import Path

except ModuleNotFoundError as exp:
    print(exp.with_traceback())

db = mgo_db_connector.get_collection('coding_exam_history')

async def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


MIN_PORT = int(os.environ['PORT_JUPYTER'])
import threading 
# Declraing a lock
lock = threading.Lock()
async def find_free_port_v2():
    global MIN_PORT
    lock.acquire()
    current = MIN_PORT
    MIN_PORT = max(int(os.environ['PORT_JUPYTER']), (MIN_PORT+1)/(int(os.environ['PORT_JUPYTER'])+100))
    lock.release()
    return current
 
async def update_data_KT():
    cmd_list = []
    dir = f'/data/owncloud'
    CMD = f'mkdir -p \'{dir}\''
    p = subprocess.Popen([CMD],stdout=subprocess.PIPE, shell=True)
    t1 = p.wait()

    cmd_list.append({'is_terminated':True if t1 ==0 else False,'cmd':p})


    CMD = f'owncloudcmd -u ai-exams -p 12345678 /data/owncloud http://118.70.48.144:9898'
    print(CMD)
    proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True) 
    t2 = proc.wait()


    cmd_list.append({'is_terminated':True if t2==0 else False,'cmd':proc})

    for cmd in cmd_list:
        if cmd.get('is_terminated'):
            cmd.get('cmd').terminate()
    return True

async def start_exams_v2_db(course_name: str , quiz_name: str,user_id:int) ->dict:
    cmd_list = []

    # QUIZ_RELEASE_PATH = f'/home/ds1/sonnm/Assiment/release/{quiz_name}/'
    # QUIZ_SUBMIT_PATH = f'/home/ds1/sonnm/AI/data/code-exams/{course_name}/submitted/{user_id}/{quiz_name}/'



    # dir = f'/data/owncloud'
    # CMD = f'mkdir -p \'{dir}\''
    # p = subprocess.Popen([CMD],stdout=subprocess.PIPE, shell=True)
    # t1 = p.wait()

    # cmd_list.append({'is_terminated':True if t1 ==0 else False,'cmd':p})


    # CMD = f'owncloudcmd -u ai-exams -p 12345678 /data/owncloud http://118.70.48.144:9898'
    # print(CMD)
    # proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True) 
    # t2 = proc.wait()


    # cmd_list.append({'is_terminated':True if t2==0 else False,'cmd':proc})



    # CMD = f'mkdir static'
    # CMD = f'owncloudcmd -u ai-exams -p 12345678 -ds /static/ http://118.70.52.237:8061'
    QUIZ_RELEASE_PATH = f'/data/owncloud/exams/Assiment/release/{quiz_name}/'
    # QUIZ_RELEASE_PATH = f'/data/owncloud/exams/code-exams/{course_name}/submitted/{user_id}/{quiz_name}'
    QUIZ_SUBMIT_PATH = f'/data/owncloud/exams/code-exams/{course_name}/submitted/{user_id}/'
    
    # print(QUIZ_RELEASE_PATH)
    # print(QUIZ_SUBMIT_PATH)
    # if(not os.path.isfile(QUIZ_RELEASE_PATH)):
    #     print("File khong ton tai")
    #     return {}

    # if check_opened_port(int(user_id)):
    #     return {'Link': f'http://118.70.52.237:{user_id}', 'Status': 'Opened'}

    # Path(f'{QUIZ_SUBMIT_PATH}').mkdir(parents=True, exist_ok=True)

    # stream = os.popen('ls -al /home/ds1/sonnm/AI/code-exams/CV-05/21/submitted/')
    # output = stream.read()
    # print(output)

    # stream = os.popen(f'chmod -R 777 /home/ds1/sonnm/AI/data/code-exams/{course_name}/submitted/{user_id} && ls -al \'{QUIZ_SUBMIT_PATH}\'')
    # # print(f'mkdir -p \'{QUIZ_SUBMIT_PATH}\' && ls -al \'{QUIZ_SUBMIT_PATH}\'')
    # output = stream.read()
    # print(output)

    CMD = f'mkdir -p \'{QUIZ_SUBMIT_PATH}\' && cp -r \'{QUIZ_RELEASE_PATH}\' \'{QUIZ_SUBMIT_PATH}\''
    # print(CMD)
    # stream = os.popen(CMD)
    # output = stream.read()
    # print(output)

    p = subprocess.Popen([CMD],stdout=subprocess.PIPE, shell=True)
    t3 = p.wait()

    cmd_list.append({'is_terminated':True if t3 ==0 else False,'cmd':p})

    # pass_hash = await get_password(user_id)
    pass_hash = ""
    lock = asyncio.Lock()
    
    async with lock:
    
        port = await find_free_port_v2()

        # sudoPassword = '' 
        # CMD_ALLOW_PORT =  f'sudo ufw allow {port}'
        #for test
        # --memory="2g" --memory-reservation="750m"
        CMD = 'docker run -it -d  --rm --memory="2g" --memory-swap="2g" --cpus="1.5" --name student_{} -v \'{}\':\'{}\' -v \'{}\':\'{}\'  -v \'{}\':\'{}\' -p {}:443 exams-jupyter'.format(
            user_id,
            QUIZ_SUBMIT_PATH ,
            '/home/jovyan/exams/source/ps1',
            '/var/run/docker.sock',
            '/var/run/docker.sock',
            os.environ['SSL_DIR'],
            '/ssl-cert/exams.aiacademy.edu.vn',
            port,
            # '/bin/cat /sys/fs/cgroup/memory/memory.limit_in_bytes'
        )
        print(CMD)
        proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True) 
        t4 = proc.wait()
        cmd_list.append({'is_terminated':True if t4 ==0 else False,'cmd':p})


        for cmd in cmd_list:
            if cmd.get('is_terminated'):
                cmd.get('cmd').terminate()
            
    # return {'Link': f'http://exams.aiacademy.edu.vn/?port={port}'}
    return {'link': f'https://{os.environ.get("DOMAIN")}:{port}/notebooks','host':'https://{}/api'.format(os.environ['DOMAIN'])}
    #return {'Link': f'https://118.70.52.237:{port}/notebooks'}

async def submit_exams_v2_db(course_name: str , quiz_name: str , user_id:int , object_id:str , quiz_id:int , time_close:int ):
    cmd_list = []

    cmd_stop_docker = f'docker stop student_{user_id}'
    #execute cmd
    proc_stop_docker = subprocess.Popen([cmd_stop_docker], stdout=subprocess.PIPE, shell=True)
    #wait until it termanate
    wait_stop_docker = proc_stop_docker.wait()
    cmd_list.append({'is_terminated':True if wait_stop_docker == 0 else False,'cmd':proc_stop_docker})

    data = {}
    data.update({
        '_id':ObjectId(object_id),
        'course_name':course_name,
        'quiz_name':quiz_name,
        'user_id':user_id,
        'quiz_id':quiz_id,
        'time_close':time_close, 
        'is_graded': False,
        'my_answers': 'Bài tập chưa được chấm. Vui lòng quay lại sau', 
        'max_score': 10.0, 
        'score': 0.0,
    })
    
    response = await db.insert_one(data)


    # CMD = f'owncloudcmd -u ai-exams -p 12345678 /data/owncloud http://118.70.48.144:9898'
    # print(CMD)
    # proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True) 
    # t5 = proc.wait()


    # cmd_list.append({'is_terminated':True if t5==0 else False,'cmd':proc})



    for cmd in cmd_list:
            if cmd.get('is_terminated'):
                cmd.get('cmd').terminate()

    return response


    
