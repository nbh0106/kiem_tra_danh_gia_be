from json import decoder
from os import path
from posix import listdir
import shutil
import sys


try:
    import pretty_errors
    from fastapi import APIRouter,Path,Depends, Body, Query
    from fastapi.security import HTTPBearer
    from fastapi_jwt_auth import AuthJWT
    
    import json
    import os
    
    import time
    import subprocess
    import socket
    from app.auth import get_password, save_score_mongo, get_coding_exams_score_mongo
    from app.server.coding_exams.get_result import get_grade_of_course

    from app.database import mgo_db_connector
    from .database import start_exams_v2_db, submit_exams_v2_db, update_data_KT
    
except Exception as exp:
    print(exp)

router = APIRouter()

CODE_STORAGE = '/home/ds1/sonnm/AI'
JUPY_PROC_LIST = {}

@router.get('/', dependencies=[Depends(HTTPBearer())])
async def get_code_exams(Authorize: AuthJWT = Depends(), course_id: str = Query(...)):
    path = f'{CODE_STORAGE}/{course_id}/source'
    try:
        directory_contents = os.listdir(path)
    except:
        return []
    exams_list = []
    for name in directory_contents :
        if os.path.isdir(f"{path}/{name}"):
            exams_list.append({'name':name})
    return exams_list


@router.post('', dependencies=[Depends(HTTPBearer())])
async def create_exams(Authorize: AuthJWT = Depends(), course_id: str = Query(...)):
    # nbgrader generate_assignment
    cmd = f'cd {CODE_STORAGE} && nbgrader quickstart {course_id}'# && rm -rf {CODE_STORAGE}/{course_id}/source/ps1' 
    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    print(proc.communicate())
    
    return {}



@router.get('/start', dependencies=[Depends(HTTPBearer())])
async def start_exams(Authorize: AuthJWT = Depends(), course_id: str = Query(...), subtask: str = Query(...)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    user_id = current_user['id']

    if check_opened_port(int(user_id)):
        return {'Link': f'http://aiacademy.edu.vn:{user_id}', 'Status': 'Opened'}
    
    create_nbgrader_user(course_id, user_id)
    
    # Copy release folder to submitted folder
    path = f'{CODE_STORAGE}/{course_id}/release/*'
    des = f'{CODE_STORAGE}/{course_id}/submitted/{user_id}'
    
    stream = os.popen(f'mkdir -p {des} && cp -r {path} {des}')
    output = stream.read()

    #myPass123
    # password = passwd()
    pass_hash = get_password(user_id)
    cmd = f'cd {des}/{subtask} && jupyter notebook --allow-root --ip 0.0.0.0  --NotebookApp.terminals_enabled=False --NotebookApp.password="{pass_hash}" --port {user_id}'
    #process = multiprocessing.Process(target=threaded_function, args=(cmd,))
    
    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True) 
    print(proc.returncode)
    if proc.returncode != None :
        return {'Status': 'Not found'}
    JUPY_PROC_LIST[user_id] = proc.pid
    
    return {'Link': f'http://aiacademy.edu.vn:{user_id}'}


@router.get('/start_v2', dependencies=[Depends(HTTPBearer())])
async def start_exams_v2(Authorize: AuthJWT = Depends(), course_name: str = Query(...), quiz_name: str = Query(...)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    user_id = current_user['id']

    r = await start_exams_v2_db(course_name, quiz_name, user_id)
    return  r


@router.get('/submit_v2', dependencies=[Depends(HTTPBearer())])
async def submit_exams_v2(Authorize: AuthJWT = Depends(), course_name: str = Query(...), quiz_name: str = Query(...)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    user_id = current_user['id']

    return await submit_exams_v2_db(course_name, quiz_name, user_id)
    
@router.post('/update_data', dependencies=[Depends(HTTPBearer())])
async def update_data(Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())

    return await update_data_KT()


@router.get('/submit', dependencies=[Depends(HTTPBearer())])
async def submit_exams(Authorize: AuthJWT = Depends(), course_id: str = Query(...), subtask: str = Query(...)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    user_id = current_user['id']

    if user_id in JUPY_PROC_LIST:
        os.system('pkill -P ' + str(JUPY_PROC_LIST[user_id]))

    user_id=str(user_id)
    cmd = f'cd {CODE_STORAGE}/{course_id} && nbgrader autograde {subtask} --force --student=\'"{user_id}"\''
    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    print(proc.communicate())
    
    score = get_grade_of_course(course_id, user_id, subtask)
    if score:
        score['created_time'] = time.time()
        score['id'] = user_id
        print(score)
        save_score_mongo(user_id, score)
    return score

def check_opened_port(port: int):
    a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    location = ("127.0.0.1", port)
    result_of_check = a_socket.connect_ex(location)
    a_socket.close()
    
    return result_of_check == 0
    
    
def create_nbgrader_user(course_id, user_id):
    cmd = f'cd {CODE_STORAGE}/{course_id} && nbgrader db student add {user_id}'
    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    print(proc.communicate())


@router.get('/history', dependencies=[Depends(HTTPBearer())])
async def get_score_history(Authorize: AuthJWT = Depends(), course_id: str = Query(...), subtask: str = Query(...)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    user_id = current_user['id']
    
    data = mgo_db_connector.grade_score.find({'student': str(user_id), 'assignment': subtask})
    r = []
    for record in data: 
         record['_id'] = str(record['_id'])
         r.append(record)
    return r

# add assignment: nbgrader db assignment add Hadoop
# generate: nbgrader generate_assignment --assignment=Hadoop