import pandas as pd
from nbgrader.api import Gradebook, MissingEntry

def get_grade_of_course(course_id,student_id, assignment_name):
    # Create the connection to the database
    with Gradebook(f'sqlite:////Code-VinAI/{course_id}/gradebook.db') as gb:

        # Loop over each assignment in the database
        for assignment in gb.assignments:
            if assignment.name != assignment_name:
                continue
            # Loop over each student in the database
            for student in gb.students:
                if student.id != str(student_id):
                    continue
                # Create a dictionary that will store information about this student's
                # submitted assignment
                score = {}
                score['max_score'] = assignment.max_score
                score['student'] = student.id
                score['assignment'] = assignment.name

                # Try to find the submission in the database. If it doesn't exist, the
                # `MissingEntry` exception will be raised, which means the student
                # didn't submit anything, so we assign them a score of zero.
                try:
                    submission = gb.find_submission(assignment.name, student.id)
                except MissingEntry:
                    score['score'] = 0.0
                else:
                    score['score'] = submission.score
                
                return score
    return None
    
if __name__ == '__main__':
    print(get_grade_of_course(58, 1515, 'Spark'))