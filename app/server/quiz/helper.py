try:
    from app.server.quiz.model.quiz import Quiz
    from app.server.quiz.model.question import Question
    from app.server.quiz.model.option import Option
    import random
    import urllib
    import app.server.quiz.database as db
    from ..course import database as coursedb
except ModuleNotFoundError as exp:
    print(exp)
def ResponseModel(code,message,data={}):
    return {
        "data":data,
        "code":code,
        "message":message
    }

def ErrorResponseModel(error,code,message):
    return {
        "error":error,
        "code":code,
        "message":message
    }
#parse from list to QuizModel   
def parse_quiz_model(quiz_db:list,quiz_history_list:list=[]):
    #get quiz from history
    is_finished = quiz_db[0] in quiz_history_list
    quiz = Quiz(is_finished=is_finished,id = quiz_db[0],name = quiz_db[1],description=quiz_db[2],time_open = quiz_db[3],time_close = quiz_db[4],time_limit = quiz_db[5])
    
    return quiz


#parse to question  
def parse_question_model(question,all_answers):
    
    #create options that map question_id
    options = [parse_answer_model(answer) for answer in all_answers if question[0] == answer[2]]
    #shuffle options list
    random.shuffle(options)
    #create question model and put options to question
    question = Question(id=question[0],name=question[1],quiz_id=question[2],question_type=question[3],options = options)

    return question
    
    

    
def parse_answer_model(answer):
    return Option(id = answer[0],name = answer[1],question_id = answer[2])


def convert_hashlink2path(filehash):
    return '/image/{}/{}/{}'.format(filehash[:2], filehash[2:4], filehash)


def format_image_link(files):
    image={}
    for file in files:
        file = list(file)
        file[2] = urllib.parse.quote(file[2])
        
        file.append(convert_hashlink2path(file[1]))
        arr = image.get(file[0], [])
        arr.append(file)
        image[file[0]] = arr
    return image   
        
    
def response(*,status_code:int,message:str,data:dict={})->dict:
    return {'status_code':status_code,'message':message,'data':data}



