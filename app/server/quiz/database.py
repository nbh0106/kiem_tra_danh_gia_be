from time import time
from typing import List

try:

    from app.database import mydb
    from app.server.quiz.helper import parse_quiz_model
    from app.server.quiz.helper import parse_question_model, format_image_link
    from app.server.quiz.model.quiz import Quiz
    from app.server.quiz.model.question import Question
    import app.server.quiz.model.quiz_history as QuizHistoryModel
    import random
    import re
    #import db
    from app.database import mgo_db_connector

    from bson.objectid import ObjectId
    from pymongo.collection import ReturnDocument
    from app.server.quiz.model.quiz_history import QuizHistoryUpdate
    from fastapi.param_functions import Body
    from ...server.common_helpers.execute_query_mysql import get as QueryGet

except Exception as exp:
    print(exp)


async def is_quiz_of_user(userid: int, quizid: int) -> bool:
    query = """
        SELECT 
            mq.name 
        FROM 
            mdl_user_enrolments mue 
        INNER JOIN  
            mdl_enrol me 
        ON 
            mue.enrolid =me.id 
        INNER JOIN 
            mdl_quiz mq 
        ON 
            mq.course = me.courseid 
        WHERE
            mq.id = {} AND userid = {}
    """.format(quizid, userid)

    result = await QueryGet(query)

    return True if result else False


async def get_all_quizes(id_course: int, user_id: int) -> list:

    query = '''
        SELECT 
            mq.id as quizId,mq.name as quizName,mq.intro as quizDescription,
            mq.timeopen as timeOpen,mq.timeclose  as timeClose,mq.timelimit as timeLimit,
            mc.shortname
        FROM 
            mdl_quiz mq 
        INNER JOIN 
            mdl_course_modules mcm
        ON 
            mq.id = mcm.instance
        INNER JOIN 
        	mdl_course mc 
        ON
        	mc.id = mcm.course 
        WHERE 
            mcm.module = 16 and
            mcm.course  = {0} and
            mcm.deletioninprogress = 0
            

            
            
    '''.format(id_course)

    quizes = await QueryGet(query=query, is_get_one=False)
    # mydb.commit()
    course_name = quizes[0][6]

    quiz_history_list = await get_all_id_quiz_via_user_id(user_id=user_id)

    quiz_list = [parse_quiz_model(quiz, quiz_history_list) for quiz in quizes]

    return {'course_name': course_name, 'quiz_list': quiz_list}


# get quiz via quiz_id
async def get_quiz_via_quiz_id(quiz_id: int) -> Quiz:

    query = '''
        SELECT 
            mq.id as quizId,mq.name as quizName,mq.intro as quizDescription,
            mq.timeopen as timeOpen,mq.timeclose  as timeClose,mq.timelimit as timeLimit
        FROM 
            mdl_quiz mq 
        WHERE 
            mq.id = {0}
    '''.format(quiz_id)

    quiz = await QueryGet(query=query)
    # get_set_of_questions_of_quiz
    questions_of_quiz = await get_all_questions(quiz_id)
    # shuffle question list
    random.shuffle(questions_of_quiz)
    # parse quiz

    quiz_parse = parse_quiz_model(quiz_db=quiz)
    quiz_parse.questions = questions_of_quiz

    # Repair image link
    image_link = await get_image_question(quiz_id)
    print(image_link)
    for question in quiz_parse.questions:
        if question.id in image_link:
            for image in image_link[question.id]:
                org = "@@PLUGINFILE.*{}".format(image[2])
                question.name = re.sub(org, image[3], question.name)
    # for test
    #quiz_parse.time_limit = 30
    return quiz_parse


async def get_all_questions(quiz_id: int):
    query = '''
        SELECT 
	        mq.id as question_id,mq.questiontext as name,mqs.quizid as quiz_id,mqmo.single as question_type 
        FROM    
            mdl_quiz_slots mqs
        INNER JOIN
            mdl_question mq 
        ON 
            mqs.questionid = mq.id 
        INNER JOIN
            mdl_qtype_multichoice_options mqmo
        ON 
            mqmo.questionid = mq.id
        WHERE
            mqs.quizid = {0}
        ORDER BY
            mq.id
    '''.format(quiz_id)

    questions = await QueryGet(query=query, is_get_one=False)
    # mydb.commit()
    all_answers = await get_all_answers(quiz_id)
    questions_parse = list(parse_question_model(
        question, all_answers) for question in questions)

    return questions_parse


async def get_all_answers(quiz_id: int) -> list:
    query = '''
        SELECT 
            mqa.id as answer_id,mqa.answer,mqa.question FROM mdl_question_answers mqa 
        INNER JOIN 
            mdl_quiz_slots mqs 
        ON 
            mqs.questionid = mqa.question 
        WHERE 
            mqs.quizid = {0}
        ORDER BY 
            questionid 
    '''.format(quiz_id)

    answers = await QueryGet(query=query, is_get_one=False)
    # mydb.commit()

    return answers


async def get_image_question(quiz_id):
    query = '''
        SELECT
            mf.itemid, mf.contenthash, mf.filename
        FROM 
            mdl_quiz_slots as mqs, mdl_files as mf
        WHERE
                mqs.quizid = {}
            and
                mqs.questionid=mf.itemid
            and
                mf.component='question'
            and
                mf.filename <> '.'
    '''.format(quiz_id)

    files = await QueryGet(query=query, is_get_one=False)
    # mydb.commit()

    return format_image_link(files)


async def get_correct_answer(quiz: Quiz) -> QuizHistoryModel.QuizHistoryUpdate:
    quiz_history = QuizHistoryModel.QuizHistoryUpdate()

    query = '''
        SELECT 
	        mqa.id 
        FROM 
	        mdl_quiz_slots mqs 
        INNER JOIN 
	        mdl_question_answers mqa 
        ON
	        mqs.questionid = mqa.question 
        WHERE 
	        mqs.quizid = {0}
        AND
	    	mqa.fraction <> 0
        '''.format(quiz.id)

    correct_answer = await QueryGet(query=query, is_get_one=False)
    # mydb.commit()

    correct_answer_id = list(map(lambda i: i[0], correct_answer))

    right_count = 0
    question_list = []
    for question in quiz.questions:
        right = True
        for option in question.options:
            if option.id in correct_answer_id:
                option.is_answer = True
            if option.is_answer != option.selected:
                right = False

        if right:
            right_count += 1
            question.is_right = True
        else:
            question.is_right = False
        question_list.append(question)

    quiz.questions = question_list
    quiz_history.quiz = quiz

    if len(quiz.questions) != 0:
        quiz_history.score = round(2*right_count*10.0/len(quiz.questions))/2
    else:
        quiz_history.score = 0
    quiz_history.question_correct = right_count
    quiz_history.question_total = len(quiz.questions)

    return quiz_history


async def get_quiz_history_via_quiz_id(user_id: int, quiz_id: int) -> dict:
    record = await mgo_db_connector.quiz_history.find_one({'quiz.id': quiz_id, 'user_id': user_id})
    if record:
        del record['_id']
    return record

# async def get_unfinished_quiz_history(user_id: int,is_finished:bool) -> QuizHistoryUpdate:

#     quiz_db = await mgo_db_connector.quiz_history.find_one({'quiz.is_finished':is_finished,'user_id': user_id})

#     return QuizHistoryUpdate(**quiz_db) if quiz_db else None


# exam has not unfinished yet
async def get_quiz_history_unfinished(user_id: int) -> QuizHistoryUpdate:

    quiz_history = await mgo_db_connector.quiz_history.find_one({'quiz.is_finished': False, 'user_id': user_id}, {'_id':0,'quiz_id': '$quiz.id', 'quiz_name': '$quiz.name', 'course_name': 'course_name'})

    return quiz_history


async def update_quiz_history(quiz_history: QuizHistoryModel.QuizHistoryUpdate = Body(...), action='update'):
    quiz_history_new = {k: v for k,
                        v in quiz_history.dict().items() if v is not None}
    is_upserted = False if action == 'update' else True
    #quiz_history_updated = mgo_db_connector.quiz_history.update_one({'quiz.id':quiz_history.quiz.id,'user_id':quiz_history.user_id,'quiz.is_finished':False},{"$set":quiz_history_new},is_upserted)
    #quiz_history_updated = mgo_db_connector.quiz_history.find_one({'quiz.id':quiz_history.quiz.id,'user_id':quiz_history.user_id})
    quiz_history_updated = await mgo_db_connector.quiz_history.find_one_and_update({'quiz.id': quiz_history.quiz.id, 'user_id': quiz_history.user_id, 'quiz.is_finished': False}, {"$set": quiz_history_new}, upsert=is_upserted, return_document=ReturnDocument.AFTER)
    return quiz_history_updated


async def get_all_id_quiz_via_user_id(user_id: int) -> list:
    db = mgo_db_connector.get_collection('quiz_history')
    quiz = db.find({'user_id': user_id, 'quiz.is_finished': True}, {
                   'id': '$quiz.id', '_id': 0})
    return [q['id'] async for q in quiz]


async def delete_quiz_history(object_id: str, user_role: int):
    allow_permision = user_role < 5
    if allow_permision:
        result_delete_quiz_exam = await mgo_db_connector.quiz_history.delete_one({'_id': ObjectId(object_id)})
        result_delete_coding_exam = await mgo_db_connector.coding_exam_history.delete_one({'_id': ObjectId(object_id)})
        if result_delete_quiz_exam and result_delete_coding_exam:
            return True
    return False


# get quiz exam and coding exam history


async def get_exam_history(object_id: str):

    def parse(exam_history, is_quiz: bool = True) -> dict:
        #is_graded = exam_history.get('is_graded')
        #is_finished =  is_graded if is_graded != None else True

        name = 'Lý thuyết' if is_quiz else 'Thực hành'

        return {'name': name, 'score': exam_history['score'], 'is_quiz': is_quiz}

    data = []

    quiz_exam_history = await mgo_db_connector.quiz_history.find_one({"_id": ObjectId(object_id)})
    data.append(parse(quiz_exam_history))

    coding_exam_history = await mgo_db_connector.coding_exam_history.find_one({"_id": ObjectId(object_id)})
    data.append(parse(coding_exam_history, is_quiz=False))

    result = {}

    result.update({'exam_history': data,
                  'quiz_name': coding_exam_history['quiz_name'], 'user_name': quiz_exam_history['user_name']})

    return result, quiz_exam_history['user_id'], quiz_exam_history['quiz']['time_close']


async def get_coding_exam_history(object_id: str):
    result = await mgo_db_connector.coding_exam_history.find_one({"_id": ObjectId(object_id)}, {"_id": 0, 'my_answers': 1, 'time_close': 1, 'quiz_name': 1, 'quiz_id': 1, 'user_id': 1})

    return result if result else None


async def get_coding_exam_solution(quiz_id: int):
    result = await mgo_db_connector.solutions.find_one({'quiz_id': quiz_id}, {'_id': 0, 'solution': 1, 'quiz_name': 1, 'time_close': 1})
    return result if result else None


async def get_time_close_of_quiz(quiz_id: int):
    query = f'''SELECT timeclose FROM mdl_quiz mq WHERE id = {quiz_id}'''
    time_close = await QueryGet(query=query)
    time_close, *_ = time_close
    return time_close
