try:
    from pydantic import BaseModel,Field
    from typing import Optional,List
    from app.server.quiz.model.option import Option
except ModuleNotFoundError as exp:
    print(exp)
    
    

class Question(BaseModel):
    id:int = Field(...)
    name:str=Field(...)
    quiz_id:int = Field(...)
    question_type:int= Field(...)
    options:List[Option] = Field(...)
    answered:Optional[bool]=Field(False)
    is_right:Optional[bool]=Field(False)
    
    
