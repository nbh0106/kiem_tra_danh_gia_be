try:
    from pydantic import BaseModel,Field
    from typing import Optional,List
    from app.server.quiz.model.question import Question
except ModuleNotFoundError as exp:
    print(exp)
    
class Quiz(BaseModel):
    is_finished:bool = Field(False)
    id:int = Field(...)
    name:str = Field(...)
    description:str = Field(...)
    time_open:int = Field(...)
    time_close:int = Field(...)
    time_limit:int = Field(...)
    questions:Optional[List[Question]]
    
   