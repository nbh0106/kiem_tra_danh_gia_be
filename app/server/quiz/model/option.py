try:
    from pydantic import BaseModel,Field
    from typing import Optional
except ModuleNotFoundError as exp:
    print(exp)
    
    
class Option(BaseModel):
    id:int= Field(...)
    question_id:int  = Field(...)
    name:str = Field(...)
    is_answer:bool = Field(False)
    selected:bool = Field(False)
    
    