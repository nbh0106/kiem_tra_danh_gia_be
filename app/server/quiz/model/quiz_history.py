try:
    from typing import Optional
    from pydantic import BaseModel,Field
    from app.server.quiz.model.quiz import Quiz
except ModuleNotFoundError as exp:
    print(exp.with_traceback)
    
class QuizHistory(BaseModel):
    course_name:str = Field(...)
    quiz:Quiz = Field(...)
    user_id:int = Field(...)
    user_name:str = Field(...)
    score:float = Field(...)
    question_correct:int = Field(...)
    question_total:int = Field(...)
    started_time:int = Field(...)
    submitted_time:int = Field(...)
    exam_link:dict = Field(...)
    
    
class QuizHistoryUpdate(BaseModel):
    course_name:Optional[str] = Field(None)
    quiz:Optional[Quiz] = Field(None)
    user_id:Optional[int]= Field(None)
    user_name:Optional[str]= Field(None)
    score:Optional[float]= Field(None)
    question_correct:Optional[int]= Field(None)
    question_total:Optional[int]= Field(None)
    started_time:Optional[int]= Field(None)
    submitted_time:Optional[int]= Field(None)
    exam_link:Optional[dict]=Field(None)