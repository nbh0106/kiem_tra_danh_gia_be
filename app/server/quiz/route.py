
from .model.quiz_history import QuizHistoryUpdate
from typing import Optional

from fastapi import APIRouter, Path, Depends, Body, Query, status, Response,HTTPException
from fastapi.security import HTTPBearer
from fastapi_jwt_auth import AuthJWT
from fastapi.encoders import jsonable_encoder
from .database import \
    get_all_quizes as get_all_quizesdb, \
    get_all_questions as get_all_questions_db, \
    get_all_answers as get_all_answers_db, \
    get_quiz_via_quiz_id,   get_correct_answer
from .model.quiz import Quiz
from .helper import ErrorResponseModel, ResponseModel
import time
import json
from ...database import mgo_db_connector
import pytz
from bson.objectid import ObjectId
import time
from . import database as db
from .model import quiz_history as QuizHistoryModel
from ..coding_exams.database import start_exams_v2_db, submit_exams_v2_db
from ..course import database as coursedb
#from ..course.helper import Course
# from typing import Optional


timezone = pytz.timezone("Asia/Ho_Chi_Minh")

router = APIRouter()


@router.get("/of_course/{course_id}", dependencies=[Depends(HTTPBearer())])
async def get_all_quizes(Authorize: AuthJWT = Depends(), course_id: int = Path(...)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())

    # course_id_list = await get_course_id_list(user=current_user)

    try:
        quiz_list = await get_all_quizesdb(id_course=course_id, user_id=current_user['id'])
        return ResponseModel(code=200, data=quiz_list, message="get list of course success")
    except IndexError as exp:
        return ErrorResponseModel(code=404, error="not found", message="Không có bài tập nào được tìm thấy")
    # if quiz_list == None:
    #     return ErrorResponseModel(code=403,error="permession denied",message="Bạn không có quyền thực hiện công việc này")
    # else:
    #     return ResponseModel(code=200,data = quiz_list,message="get list of course success")

#need course_name and quiz_name because api for get link jupyter 
@router.get("/{quiz_id}", dependencies=[Depends(HTTPBearer())])
async def get_quiz_by_id(Authorize: AuthJWT = Depends(),quiz_id: int = Path(...), course_name: Optional[str] = Query(None), quiz_name: Optional[str] = Query(None)):
    # 1.first,get quiz through conditions about time
    # get record via quiz_id
    # if submitted time is none then return this old quiz
    # else return error:'this quiz have been exam '
    # if none:
    # if is there any record that have submitted time is none then return that quiz
    # else return  result that returned then return new quiz with warning
    # if time of now is between open time and close time
    # then insert into db about:start_time(seconds),user_name,user_id
    # else return error 'time interval violation'

    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    # for auth
    # check whether user has permission for quiz
    is_quiz_of_user = await db.is_quiz_of_user(current_user["id"], quiz_id)
    if not is_quiz_of_user:
        raise HTTPException(status_code=403,detail="Bạn không có quyền")
    # get quiz in quiz history to see whether that quiz have been finished or not 
    record = await db.get_quiz_history_via_quiz_id(quiz_id=quiz_id, user_id=current_user['id'])  
    if record:

        is_finished = record['quiz']['is_finished']
        #if that quiz have not finished then keep going to finished that exam 
        #not allow to do 2 exam in the same time 
        if not is_finished:
            return ResponseModel(data=record, code=202, message='keep going with this quiz')
        else:
            raise HTTPException(status_code=405, detail="this quiz have been exam")
    else:
        #check whether has an exists exam that unfinished
        record = await db.get_quiz_history_unfinished(user_id=current_user['id'])

        if record:
    
            return ResponseModel(data=record, code=406, message='another exam have not been finished')
        else:
            #get multi-choice part
            record = await db.get_quiz_via_quiz_id(quiz_id=quiz_id)
            # for coding jupyter apart
            examCodingLink = await start_exams_v2_db(course_name=course_name, quiz_name=quiz_name, user_id=current_user['id'])
            # record is Quiz type
            now = time.time()

            # if (current_user['role'] < 5) or ((record.time_close == 0) and (record.time_open <= now)) or ((record.time_close != 0) and (record.time_open <= now) and (now < record.time_close + 60*5)):
            if True:
                # insert to db
                # for test
                #record.time_limit = 60

                quiz_history = QuizHistoryUpdate()
                quiz_history.course_name = course_name
                quiz_history.user_id = current_user['id']
                quiz_history.user_name = current_user['name']
                quiz_history.started_time = now
                quiz_history.quiz = record
                # for coding exam

                quiz_history.exam_link = examCodingLink

                await db.update_quiz_history(quiz_history=quiz_history, action='insert')

                return ResponseModel(data=quiz_history, code=200, message='get quiz successfull')
            else:
                raise HTTPException(status_code=406,detail="time interval violation")


@router.put("/update_history", dependencies=[Depends(HTTPBearer())])
async def update_history(Authorize: AuthJWT = Depends(), quiz: Quiz = Body(...)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    quiz_history = QuizHistoryModel.QuizHistoryUpdate()
    quiz_history.quiz = quiz
    quiz_history.user_id = current_user['id']
    quiz_history_update = await db.update_quiz_history(quiz_history=quiz_history)
    if quiz_history_update:
        return ResponseModel(code=200, message="update successfull")
    else:
        return ErrorResponseModel(code=404, error="", message="can not update")


@router.post("/submit", dependencies=[Depends(HTTPBearer())])
async def submit(Authorize: AuthJWT = Depends(), quiz: Quiz = Body(...), course_name: Optional[str] = Query(None), quiz_name: Optional[str] = Query(None)):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())
    submitted_time = time.time()

    # check submitted time valid
    quiz_db = await db.get_quiz_history_via_quiz_id(user_id=current_user['id'], quiz_id=quiz.id)
    started_time = quiz_db.get('started_time')
    limited_time = quiz.time_limit
    time_is_valid = submitted_time <= started_time + limited_time + 10

    quiz_history = await db.get_correct_answer(quiz)
    quiz_history.score = quiz_history.score if time_is_valid else 0
    quiz_history.submitted_time = submitted_time
    quiz_history.user_id = current_user['id']
    quiz_history.user_name = current_user['name']
    quiz_history.quiz.is_finished = True

    quiz_history_update = await db.update_quiz_history(quiz_history=quiz_history)
    object_id = str(quiz_history_update["_id"])

    response_from_insert_coding_exam_to_db = await submit_exams_v2_db(quiz_id=quiz_history_update['quiz']['id'], object_id=object_id, course_name=course_name, quiz_name=quiz_name, user_id=current_user['id'], time_close=quiz.time_close)

    if quiz_history_update and response_from_insert_coding_exam_to_db:
        return ResponseModel(code=200, data=str(quiz_history_update['_id']), message="insert successfull")
    else:
        return ErrorResponseModel(code=404, error="", message="insert not success")


@router.get("/{object_id}/history_detail", dependencies=[Depends(HTTPBearer())], status_code=status.HTTP_200_OK)
async def quiz_history_detail(Authorize: AuthJWT = Depends(), object_id: str = Path('608e77c319a71d7afee77076'), response: Response = status.HTTP_200_OK):
    # print('hello')
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())

    quiz = await mgo_db_connector.quiz_history.find_one({'_id': ObjectId(object_id), 'quiz.is_finished': True})

    time_close = quiz['quiz']['time_close']
    now = time.time()
    # print('finished')
    # if  (current_user['role'] < 5) or ((quiz['user_id'] == current_user['id'] or current_user['role'] < 5) and time_close < now):
    if True:
        quiz.update({'oid': str(quiz.get('_id'))})
        del quiz['_id']
        return ResponseModel(code=200, data=quiz, message="get history quiz success")
    else:
        response.status_code = status.HTTP_403_FORBIDDEN
        # return ErrorResponseModel(code=400,error="",message="not permission")


@router.get("/{quiz_id}/history", dependencies=[Depends(HTTPBearer())], status_code=status.HTTP_200_OK)
async def quiz_history(Authorize: AuthJWT = Depends(), quiz_id: int = Path(...), response: Response = status.HTTP_200_OK):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())

    now = time.time()

    result = []
    if current_user['role'] >= 5:
        result = await quiz_user(current_user['id'], quiz_id)
        print(result)
        if len(result) > 0:
            time_close = result[0]['time_close']
            # if now < time_close :
            if False:
                result = []
                response.status_code = status.HTTP_403_FORBIDDEN
                return result

    else:
        result = await quiz_admin(quiz_id)

    # if len(result) < 1:
    #     response.status_code = status.HTTP_403_FORBIDDEN

    return result


@router.delete("/delete/{object_id}", dependencies=[Depends(HTTPBearer())], status_code=status.HTTP_200_OK)
async def delete_quiz_history(Authorize: AuthJWT = Depends(), object_id: str = Path(...), response: Response = status.HTTP_200_OK):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())

    quiz_delete = await db.delete_quiz_history(object_id=object_id, user_role=current_user['role'])
    if quiz_delete:
        return ResponseModel(code=204, message="delete successfull")
    else:
        response.status_code = status.HTTP_403_FORBIDDEN
        return ErrorResponseModel(code=403, error="", message="delete not successfull")

# api for get exam that include: multichoice exam and coding exam history


@router.get('/exam_history/{object_id}', summary='get both multichoice exam and coding exam history', dependencies=[Depends(HTTPBearer())], status_code=status.HTTP_200_OK)
async def get_exam_history(object_id: str = Path(...), Authorize: AuthJWT = Depends(), response: Response = status.HTTP_200_OK):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())

    result, user_id, time_close_quiz = await db.get_exam_history(object_id=object_id)

    now = time.time()

    # if  (current_user['role'] < 5) or (result and current_user['id'] == user_id and time_close_quiz < now ):
    if True:
        return ResponseModel(code=200, data=result, message="get exam history success")
    else:
        response.status_code = status.HTTP_403_FORBIDDEN
        return ErrorResponseModel(code=400, error="", message="can not get exam history")


@router.get('/my_coding_exam_history/{object_id}', summary="get my answers of coding exam", dependencies=[Depends(HTTPBearer())], status_code=status.HTTP_200_OK)
async def get_my_coding_exam_history(object_id: str = Path(...), Authorize: AuthJWT = Depends(), response: Response = status.HTTP_200_OK):
    Authorize.jwt_required()
    current_user = json.loads(Authorize.get_jwt_subject())

    result = await db.get_coding_exam_history(object_id=object_id)
    user_id = result['user_id']

    time_close_quiz = result['time_close']
    now = time.time()

    if (current_user['role'] < 5) or (result and current_user['id'] == user_id and time_close_quiz < now):
        return ResponseModel(code=200, data=result, message="get my answer of coding exam success")
    else:
        response.status_code = status.HTTP_403_FORBIDDEN
        return ErrorResponseModel(code=400, error="", message="can not get coding exam history data")


@router.get('/solution/{quiz_id}', summary="get solution of coding exam", dependencies=[Depends(HTTPBearer())], status_code=status.HTTP_200_OK)
async def get_coding_exam_solution(quiz_id: int = Path(...), Authorize: AuthJWT = Depends(), response: Response = status.HTTP_200_OK):
    Authorize.jwt_required()

    result = await db.get_coding_exam_solution(quiz_id=quiz_id)
    time_close_quiz = result['time_close']
    now = time.time()
    if result and now > time_close_quiz:
        return ResponseModel(code=200, data=result, message="get solution of coding exam success")
    else:
        response.status_code = status.HTTP_403_FORBIDDEN
        return ErrorResponseModel(code=400, error="", message="can not get solution of coding exam")


async def quiz_user(user_id: int, quiz_id: int):
    result = await mgo_db_connector.quiz_history.find_one({'quiz.id': quiz_id, 'user_id': user_id, 'quiz.is_finished': True}, {'_id': 0, 'oid': '$_id', 'quiz_name': '$quiz.name', 'user_name': 1, 'started_time': 1, 'submitted_time': 1, 'time_close': '$quiz.time_close'})
    result_list = []

    if result:
        result['oid'] = str(result['oid'])
        result_list.append(result)
    return result_list


async def quiz_admin(quiz_id):
    result = mgo_db_connector.quiz_history.find({'quiz.id': quiz_id, 'quiz.is_finished': True}, {
                                                '_id': 0, 'oid': '$_id', 'quiz_name': '$quiz.name', 'user_name': 1, 'started_time': 1, 'submitted_time': 1}).sort([('started_time', -1)])

    result_list = []
    async for r in result:
        r['oid'] = str(r['oid'])
        result_list.append(r)
    return result_list
