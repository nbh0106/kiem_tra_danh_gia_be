
import bcrypt
from .database import  mgo_db_connector,connection_pool
from bson.objectid import ObjectId
from notebook.auth import passwd
from .server.common_helpers.execute_query_mysql import get as QueryGet
import asyncio

async def get_user(username):
    
    query = "SELECT password, id, firstname, lastname FROM mdl_user where username='{}'".format(username)
    record =await QueryGet(query=query)

    user = list(record) if record else []
    # closing database connection.

    if len(user) > 0:
        return user[0], user
    return None, None

async def check_auth(username='', usr_pwd=''):
    real_pwd, user = await get_user(username)
    if real_pwd == None:
        return False, None
    return bcrypt.checkpw(usr_pwd.encode('utf8'), real_pwd.encode('utf8')), user

async def get_user_role(user_id):
    if  user_id == 2: 
        return 1
    
    query = "select roleid from mdl_role_assignments mra where userid = {}".format(user_id)

    #print('UID', user_id)
    
    record = await QueryGet(query=query)

    user = list(record)
    
    return user[0]


async def save_info_mongo(id, name, password):
    collection = mgo_db_connector.user_info
    await collection.update_one(
        {'user_id':id},
        {'$set': {
                'user_name': name,
                'password': passwd(password,algorithm='sha1'),
            },
        },
        upsert=True
    )
    return


async def save_score_mongo(id, score):
    collection = mgo_db_connector.grade_score
    await collection.update_one(
        {'created_time':score['created_time']},
        {'$set': score,
        },
        upsert=True
    )
    # score.update({"_id":ObjectId()})
    # collection.insert_one(score)
    
    return None

async def get_coding_exams_score_mongo(student_id, course_id, assignment):
    data = await mgo_db_connector.grade_score.find({})
                        #    , 
                        #    {'_id':0, 'assignment':1, 'created_time':1, 'max_score': 1, 'score': 1, 'student': 1})
    
    return data



async def get_password(id):
    collection = mgo_db_connector.user_info
    user = await collection.find_one(
        {'user_id':id},
    )
    # print(user)
    return user['password']

if __name__ == '__main__':
    print(check_auth('son20112074@gmail.com', 'son20112074'))   