import asyncio
import aiomysql


async def test_example():
    pool = await aiomysql.create_pool(host='127.0.0.1', port=3307,
                                      user='root', password='123456789',
                                      db='moodle')
    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute("SELECT 42;")
            #print(cur.description)
            r = await cur.fetchone()
            r = list(r) if r else None
            print(r)
    pool.close()
    await pool.wait_closed()
    return r


loop = asyncio.get_event_loop()
loop.run_until_complete(test_example())