import shlex
import subprocess
import sys
import asyncio
import ast
import time
import os
from optparse import OptionParser


def gen_unittest_code_cplusplus(func_name, params, main_func):
    # header of file C++
    header = "#include <iostream>\n#include <string>\n#include <cxxopts.hpp>\n#include <thread>\n#include <assert.h>"

    options = "cxxopts::Options options(\"AIV\", \"Kiem tra danh gia\");\n\toptions.add_options()\n"

    declaration = '\n'

    params_str = []
    for param in params:
        ten_dau_vao = param['ten_dau_vao']
        kieu_du_lieu = param['kieu_du_lieu']
        data_type = ""
        if ten_dau_vao != 'output':
            params_str.append(ten_dau_vao)

        if kieu_du_lieu == "integer":
            data_type = "int"
        if kieu_du_lieu == "string":
            data_type = "std:string"
        if kieu_du_lieu == "float":
            data_type = "float"
        if kieu_du_lieu == "boolean":
            data_type = "bool"
        if kieu_du_lieu == "array[integer]":
            data_type = "std::vector<int>"
        if kieu_du_lieu == "array[float]":
            data_type = "std::vector<float>"
        if kieu_du_lieu == "array[boolean]":
            data_type = "std::vector<bool>"
        if kieu_du_lieu == "array[string]":
            data_type = "std::vector<std:string>"

        options += '\t("{}", "", cxxopts::value<{}>())\n'.format(ten_dau_vao, data_type)
        declaration += '    {} {} = result["{}"].as<{}>();\n'.format(
            data_type, ten_dau_vao, ten_dau_vao, data_type)

    caller = '{0}({1})'.format(func_name, ", ".join(params_str))

    main = '\
    int main (int argc, char *argv[]) {{ \
    \n    {}\t;\
    \n    auto result = options.parse(argc, argv); \
    \n    {}  \
    \n    auto r = {}; \
    \n    std::cout <<r;\
    \n    return 0;      \
    \n}}'

    main = main.format(options, declaration, caller, caller)
    test_file = f'{header}\n{main_func}\n{main}'
    # prppint(time.time() - start)
    with open(f'src/{func_name}.cc', 'w') as f:
        f.write(test_file)


payload = {
    'func_name': 'sum',
    "params": [
        {
            "ten_dau_vao": "a",
            "kieu_du_lieu": "array[integer]",
        },
        {
            "ten_dau_vao": "n",
            "kieu_du_lieu": "integer",
        },

    ],
    "function": "\
            int sum(std::vector<int> a,int n)\
        \n{int sum = 0;\
        \n for (int i=0;i<n;++i)\
        \n{\
        \n    sum+=a[i];\
        \n}\
            return sum;}           \
                       ",
    "testcase_list": [
        {
            "id": "1",
            "input": [
                {
                    "ten_dau_vao": "a",
                    "kieu_du_lieu": "array[integer]",
                    "gia_tri": "1,2,3,4,5"
                },
                {
                    "ten_dau_vao": "n",
                    "kieu_du_lieu": "integer",
                    "gia_tri": "5"
                }
            ],
            "output": 15,
            "hidden": False,
        },

        {
            "id": "1",
            "input": [
                {
                    "ten_dau_vao": "a",
                    "kieu_du_lieu": "array[integer]",
                    "gia_tri": "1,1,1,1,1"
                },
                {
                    "ten_dau_vao": "n",
                    "kieu_du_lieu": "integer",
                    "gia_tri": "5"
                }
            ],
            "output": 5,
            "hidden": False,
        }

    ]
}


async def run_test(payload):

    output_result = []

    gen_unittest_code_cplusplus(
        payload["func_name"], payload["params"], payload["function"])

    # Build C++ file of user
    CMD = "g++ src/{}.cc -Wall -fconcepts -o bin/{}".format(
        payload["func_name"], payload["func_name"])
    build_proc = await asyncio.create_subprocess_exec(*shlex.split(CMD), stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
    _, compile_error = await build_proc.communicate()

    if compile_error.decode() != '':
        return {'compile_error': compile_error.decode()}

    def gen_argument_valid(
        ten_dau_vao): return f'--{ten_dau_vao}' if (len(ten_dau_vao) > 1) else f'-{ten_dau_vao}'

    # Run with testcase

    for testcase in payload['testcase_list']:
        params_string = ' '.join(
            (f'{gen_argument_valid(param["ten_dau_vao"])} {param["gia_tri"]}' for param in testcase['input']))
        cmd_run_testcase = 'bin/{}  {}'.format(
            payload["func_name"], params_string)
        result = await asyncio.create_subprocess_exec(*shlex.split(cmd_run_testcase), stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)

        outs, errs = await result.communicate()

        outs = ast.literal_eval(outs.decode())

        data_returned = {"id": testcase["id"],
                         "expected_ouput": testcase["output"],
                         "output": outs,
                         "score": 1 if outs == testcase['output'] else 0,
                         "console_log": errs.decode(),
                         "hidden": testcase["hidden"], }
        output_result.append(data_returned)

    print(output_result)


if __name__ == "__main__":
    asyncio.run(run_test(payload))
    
